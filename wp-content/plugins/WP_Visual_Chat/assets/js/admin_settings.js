jQuery(document).ready(function() {
    jQuery('.colorpick').each(function() {
        var $this = jQuery(this);
        jQuery(this).colpick({
            color: $this.val().substr(1, 7),
            onChange: function(hsb, hex, rgb, el, bySetColor) {
                jQuery(el).val('#' + hex);
            }
        });
    });
    if(!jQuery('body').is('.toplevel_page_vcht-console')){
      jQuery('.bootstraped').find('.button').removeClass('button').addClass('btn').addClass('btn-default');
        jQuery('.bootstraped').find('.button-primary').removeClass('button-primary').addClass('btn').addClass('btn-primary');
      jQuery("[data-toggle='switch']").wrap('<div class="switch" />').parent().bootstrapSwitch({
          onLabel: vcht_textsAdmin['ON'],
          offLabel: vcht_textsAdmin['OFF']
      });
    }
});
function vcht_customFilter(){
    var content = jQuery('#filter_custom').val();
    if (content.length>0){
        document.location.href = 'admin.php?page=vcht-logsList&search='+content;
    } else {
        document.location.href = 'admin.php?page=vcht-logsList';
    }
}
function vcht_editSentence(sentenceID){
    jQuery('#vcht_winEditSentence').attr('data-sentenceid',sentenceID);
    if(sentenceID>0){        
        jQuery.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'vcht_editSentence',
                sentenceID:sentenceID
            },
            success: function (rep) {           
                var sentence = jQuery.parseJSON(rep);
                jQuery('#vcht_winEditSentence [name="sentence_title"]').val(sentence.title);
                jQuery('#vcht_winEditSentence [name="sentence_content"]').val(sentence.content);
                jQuery('#vcht_winEditSentence [name="sentence_shortcut"]').val(sentence.shortcut);
                jQuery('#vcht_winEditSentence').modal('show');
            }
        }); 
    } else {
        jQuery('#vcht_winEditSentence [name="sentence_title"]').val('');
        jQuery('#vcht_winEditSentence [name="sentence_content"]').val('');
        jQuery('#vcht_winEditSentence [name="sentence_shortcut"]').val('');
        jQuery('#vcht_winEditSentence').modal('show');
    }    
}
function vcht_saveSentence(){
    var error = false;
    if(jQuery('#vcht_winEditSentence [name="sentence_title"]').val() == ''){
        error = true;
        jQuery('#vcht_winEditSentence [name="sentence_title"]').parent().addClass('has-error');
    }
    if(jQuery('#vcht_winEditSentence [name="sentence_content"]').val() == ''){
        error = true;
        jQuery('#vcht_winEditSentence [name="sentence_content"]').parent().addClass('has-error');
    }
    if(!error){        
        jQuery('#vcht_winEditSentence').modal('hide');
        var sentenceID = jQuery('#vcht_winEditSentence').attr('data-sentenceid');
         jQuery.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'vcht_saveSentence',
                sentenceID:sentenceID,
                title: jQuery('#vcht_winEditSentence [name="sentence_title"]').val(),
                shortcut: jQuery('#vcht_winEditSentence [name="sentence_shortcut"]').val(),
                content: jQuery('#vcht_winEditSentence [name="sentence_content"]').val()
            },
            success: function () {  
                document.location.href = document.location.href;
            }
        });
    }
}