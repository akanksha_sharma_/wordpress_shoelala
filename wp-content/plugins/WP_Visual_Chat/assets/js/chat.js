// Sent from php : vcht_settings, vcht_user,vcht_userPic; vcht_ajaxurl,vcht_texts
// sent from php : vcht_url,vcht_position,vcht_pageUrl
var vcht_selectionMode = false;
var vcht_documentBody;
var vcht_avatarSel = false;
var nua = navigator.userAgent;

var vcht_username = false;
var vcht_email  = "";
var vcht_operatorID = 0;
var vcht_logID = 0;
var vcht_timer;
var vcht_timerBounce = false;
var vcht_isChatting = false;
var vcht_started = false;
var vcht_defaultOverflow = 'auto';
var vcht_lifeID = 0;
var vcht_ip = "Anonymous "+Math.floor((Math.random() * 100) + 1);

var vcht_isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return (vcht_isMobile.Android() || vcht_isMobile.BlackBerry() || vcht_isMobile.iOS() || vcht_isMobile.Opera() || vcht_isMobile.Windows());
    }
};

jQuery(document).ready(function () {
    vcht_ajaxurl = vcht_ajaxurl[0];
    if (!vcht_isIframe()) {
        if (!sessionStorage.hideChat || sessionStorage.hideChat == "false") {
            vcht_timer = false;
            vcht_user = vcht_user[0];
            vcht_username = vcht_user;
            vcht_userPic = vcht_userPic[0];
            vcht_userID = vcht_userID[0];
            vcht_userEmail = vcht_userEmail[0];
            vcht_email =  vcht_userEmail[0];
            vcht_initPanel();
        }
    }
});
jQuery(window).load(function () {
    if (!vcht_isIframe()) {
        vcht_initTimerLife();

        if (sessionStorage.vcht_username) {
            vcht_username = sessionStorage.vcht_username;
        }
        if (sessionStorage.vcht_operatorID) {
            vcht_operatorID = parseInt(sessionStorage.vcht_operatorID);
        }
        if (sessionStorage.vcht_userID) {
            vcht_userID = parseInt(sessionStorage.vcht_userID);
        }
        if (sessionStorage.vcht_logID) {
            vcht_logID = parseInt(sessionStorage.vcht_logID);
        }
        if (sessionStorage.vcht_email) {
            vcht_email = (sessionStorage.vcht_email);
        }

        if (vcht_logID > 0) {
            vcht_initChat();
            vcht_started = true;
        }

        if (jQuery(window).width() <= 769 && vcht_isMobile.any()) {
            jQuery('#chatContent').addClass('mob');
            jQuery('#chatPanel').addClass('mob');
            jQuery('#chatHistory').css({
                minHeight: jQuery('#chatContent').outerHeight() - 140
            });
            jQuery('#chatWrite textarea').css({
                width: jQuery('#vcht_chatframe').width() - 92
            });
        }


        jQuery(window).resize(function () {
          setTimeout(function(){
            if (jQuery(window).width() < 769) {
                jQuery('#chatContent').addClass('mob');
                jQuery('#chatHistory').css({
                    minHeight: jQuery('#chatContent').outerHeight() - 140
                });
                jQuery('#chatWrite textarea').css({
                    width: jQuery('#vcht_chatframe').width() - 92
                });

            } else {
                jQuery('#chatContent').removeClass('mob');

                jQuery('#chatWrite textarea').css({
                    width: 184
                });
            }
          },250);
        });

        jQuery('#chatHeader').click(function () {
            if (jQuery(this).is('.open')) {
                jQuery(this).removeClass('open');
                jQuery(this).parent().removeClass('open');
                jQuery('.nicescroll-rails').addClass('hidden');
                jQuery('body').removeClass('vcht_open');
                jQuery('#vcht_chatframe').removeClass('vcht_open');                
                jQuery('#vcht_iconMinify').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
                if (jQuery('#chatPanel').is('.mob')) {
                    jQuery('body').css('overflow-y', vcht_defaultOverflow);
                }
                vcht_initBounce();
                jQuery('.vcht_avatarSel').fadeOut(200);
                jQuery('.vcht_selectedDom').removeClass('vcht_selectedDom');
                setTimeout(function () {
                    jQuery('.vcht_avatarSel').remove();

                }, 500);

            } else {
                if (!vcht_started) {
                    vcht_initChat();
                    vcht_started = true;
                }
                jQuery('#vcht_chatframe').prop('style', '');
                jQuery('#vcht_chatframe').attr('style', '');
                if (jQuery('#chatPanel').is('.mob')) {
                    vcht_defaultOverflow = jQuery('body').css('overflow-y');
                    jQuery('body').css('overflow-y', 'hidden');
                }
                clearInterval(vcht_timerBounce);
                jQuery(this).addClass('open');
                jQuery(this).parent().addClass('open');
                setTimeout(function () {
                    jQuery('.nicescroll-rails').removeClass('hidden');

                }, 400);

                if (jQuery('#chatPanel').is('.mob')) {
                    jQuery('#chatHistory').css({
                        height: jQuery(window).height()-(45+87)
                    });
                    setTimeout(function () {
                        jQuery('#chatHistory').css({
                            height: jQuery(window).height()-(45+87)
                        });
                    }, 800);
                }
                jQuery('body').addClass('vcht_open');
                jQuery('#vcht_chatframe').addClass('vcht_open');
                jQuery('#vcht_iconMinify').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            }
        });
        if (!vcht_isMobile.any()) {
            /*   jQuery('#chatContent').niceScroll({
             cursorwidth: 10,
             autohidemode: false
             });*/
            jQuery('.nicescroll-rails').addClass('hidden');
            vcht_initBounce();

        }
    } else {
        vcht_documentBody = ((jQuery.browser.chrome) || (jQuery.browser.safari)) ? document.body : document.documentElement;
        if (!vcht_isIframe()) {
            if (vcht_elementShow != "" && vcht_elementShow.length > 0) {
                vcht_showElement(vcht_elementShow);
            }
        }
        jQuery('*').click(function (e) {
            if (vcht_selectionMode) {
                if (jQuery(this).children().length == 0 || jQuery(this).is('a') || jQuery(this).is('button') || jQuery(this).is('img') || jQuery(this).is('select')) {
                    e.preventDefault();
                    jQuery('.vcht_selectedDom').removeClass('vcht_selectedDom');
                    jQuery(this).addClass('vcht_selectedDom');
                    window.parent.vcht_selectDomElement(this);
                    vcht_selectionMode = false;
                }

            }
        });
    }
    jQuery('#vcht_chatframe').animate({opacity: 1}, 500);

});


function vcht_initTimerLife() {
  if(vcht_settings.enableInitiate == 1){
    vcht_sendLife();
    vcht_timerLife = setInterval(vcht_sendLife, 20000);
  }
}

function vcht_sendLife() {
    var name = vcht_username;
    if (name == "") {
        name = vcht_ip;
    }
    if (sessionStorage.vcht_lifeID) {
        vcht_lifeID = sessionStorage.vcht_lifeID;
    }
    jQuery.ajax({
        url: vcht_ajaxurl,
        type: 'post',
        data: {
            action: 'vcht_lifeTimer',
            name: name,
            vcht_id: vcht_lifeID
        },
        success: function (lifeID) {
            if (lifeID == 'operator') {
                clearInterval(vcht_timerLife);
                sessionStorage.vcht_lifeID = 0;
            } else if (lifeID.substr(0, 1) == '*') {
                vcht_logID = lifeID.substr(1, lifeID.length);
                sessionStorage.vcht_logID = vcht_logID;
                vcht_isChatting = true;
                jQuery.ajax({
                    url: vcht_ajaxurl,
                    type: 'post',
                    data: {
                        action: 'vcht_geo',
                        logID: vcht_logID
                    }
                });
                if (!vcht_timer) {
                    vcht_initTimer();
                }
            } else {
                vcht_lifeID = lifeID;
                sessionStorage.vcht_lifeID = lifeID;
            }
        }
    });
}

function vcht_initPanel() {
    styleMail = '';
    if (vcht_userEmail && vcht_userEmail != "") {
        styleMail = 'display: none;';
    }
    if(vcht_email == undefined){
      vcht_email = "";
    }
    var $frame = jQuery('<div id="vcht_chatframe" class="vcht_' + vcht_position + ' bootstraped" style="opacity:0;"></div>');
    $frame.append('<div id="chat" class="vcht_chat ">' +
        '<div id="chatPanel" class="container-fluid vcht_chat">' +
        '<div id="chatHeader" class="palette palette-turquoise">' +
        '<span class="fui-chat"></span>' +
        vcht_texts[1] +
        '<span id="vcht_iconMinify" class="glyphicon glyphicon-chevron-up"></span>' +
        ' </div>' +
        ' <div id="chatContent">' +
        ' <span id="vcht_loader" class="ouro ouro2">' +
        '    <span class="left"><span class="anim"></span></span>' +
        '    <span class="right"><span class="anim"></span></span>' +
        ' </span>' +
        '  <div id="chatIntro">' +
        ' <div class="row-fluid">' +
        '    <div class="col-md-12">' +
        '     <div class="text-center">' +
        '    <div class="avatarImg" style="background-image: url(' + vcht_settings.chatLogo + ');"></div>' +
        ' </div>' +
        '  <div class="form-group" id="chatIntro_formGroup">' +
        '      <input type="text" class="form-control" value="' + vcht_username + '" placeholder="' + vcht_texts[2] + '" id="chatIntro_username">' +
        '         <label class="chatIntro_username_icon fui-user" for="chatIntro_username"></label>' +
        '    </div>' +
        '  <div class="form-group" id="chatIntro_formGroup">' +
        '      <input type="text" class="form-control" value="' + vcht_email + '" placeholder="' + vcht_texts[7] + '" id="chatIntro_email">' +
        '         <label class="chatIntro_username_icon fui-mail" for="chatIntro_email"></label>' +
        '    </div>' +
        '     <a class="btn btn-primary btn-lg btn-block" href="javascript:" onclick="vcht_validUsername();">' + vcht_texts[3] + '</a>' +
        '   </div>' +
        '  </div>' +
        '  </div>' +
        '   <div id="chatRoom">' +
        ' <div id="chatHistory"></div>' +
        '  <div id="chatWrite" class="palette palette-clouds">' +
        ' <div class="form-group">' +
        '  <textarea class="form-control" rows="3"></textarea>' +
        '  <a href="javascript:" onclick="vcht_sendMessage();" class="btn btn-default"><span class="fui-chat"></span></a>' +
        '</div>' +
        ' </div>' +
        '</div>' +
        ' <div id="emailForm">' +
        ' <div class="row-fluid">' +
        '   <div class="col-md-12">' +
        ' <p>' + vcht_texts[6] + '</p>' +
        ' <div class="form-group" style="' + styleMail + '">' +
        '  <input type="text" id="chatEmail_phone" class="form-control" placeholder="' + vcht_texts[11] + '"/>' +
        '   </div>' +
        '  <div class="form-group">' +
        '   <textarea  id="chatEmail_msg" class="form-control" rows="3" placeholder="' + vcht_texts[8] + '"></textarea>' +
        '  </div>' +
        '  <a href="javascript:" onclick="vcht_sendEmail();" class="btn btn-default"><span class="fui-mail"></span>' + vcht_texts[9] + '</a>' +
        '  </div>' +
        ' </div>' +
        ' </div>' +
        '  </div>' +
        ' </div>');

    jQuery('body').append($frame);


}

function vcht_initBounce() {
    if (vcht_settings.bounceFx == '1') {
        if (vcht_timerBounce) {
            clearInterval(vcht_timerBounce);
        }
        vcht_timerBounce = setInterval(function () {
            if (!jQuery('#vcht_chatframe').is('.vcht_open')) {
                jQuery('#vcht_chatframe').css('height', 15);
                setTimeout(function () {
                    if (!jQuery('#vcht_chatframe').is('.vcht_open')) {
                        jQuery('#vcht_chatframe').css('height', 45);
                    }
                }, 800);
            }
        }, 5800);
    }
}

function vcht_initTimer() {
    if (vcht_isChatting) {
        if (isNaN(vcht_logID)) {
            vcht_logID = 0;
        }
        vcht_timer = setTimeout(function () {
            jQuery.ajax({
                url: vcht_ajaxurl,
                type: 'post',
                data: {
                    action: 'vcht_check_user_chat',
                    logID: vcht_logID
                },
                success: function (repS) {
                    if (repS.indexOf('}') > 0) {
                        var rep = jQuery.parseJSON(repS);
                        vcht_lastRep = rep;

                        jQuery.each(rep.messages, function () {
                            req = this;
                            vcht_operatorSay(req);
                        });

                        if (rep.finished == 1) {
                            vcht_isChatting = false;
                            clearTimeout(vcht_timer);
                            vcht_logID = 0;
                            sessionStorage.vcht_logID = (vcht_logID);
                            var bubble = jQuery('<div class="bubble_left palette palette-clouds"></div>');
                            if (vcht_operatorID == 0 && vcht_settings.chatLogo != "") {
                                bubble.append('<img src="' + vcht_settings.chatLogo + '" alt="" class="bubble_photo" />');
                            }
                            bubble.append(vcht_texts[5]);
                            jQuery('#chatHistory').append(bubble);
                            bubble.fadeIn(250);
                            jQuery('#chatWrite').fadeOut(250);
                            jQuery('#chatContent').scrollTop(jQuery('#chatContent')[0].scrollHeight);
                            if (jQuery('#chatPanel').is('.mob')) { if (jQuery('#chatPanel').is('.mob')) { jQuery('#chatHistory').scrollTop(jQuery('#chatHistory')[0].scrollHeight); } }
                            jQuery('.vcht_selectedDom').removeClass('vcht_selectedDom');
                            jQuery('.vcht_avatarSel').fadeOut(200);
                            setTimeout(function () {
                                document.location.href = document.location.href;
                            }, 2600);
                        }
                    }
                    setTimeout(function () {
                        vcht_initTimer();
                    }, 1500);

                }
            });
        }, 1000);
    }
}
function vcht_initChat() {
    if (isNaN(vcht_logID)) {
        vcht_logID = 0;
    }
    jQuery('#vcht_loader').fadeIn(100);
    if (vcht_user || vcht_username) {
        jQuery('#chatIntro').hide();
        vcht_startChat();
        if (vcht_logID > 0) {
            jQuery('#vcht_chatframe').prop('style', '');
            jQuery('#vcht_chatframe').attr('style', '');
            clearInterval(vcht_timerBounce);
            jQuery(this).addClass('open');
            jQuery(this).parent().addClass('open');
            setTimeout(function () {
                jQuery('.nicescroll-rails').removeClass('hidden');
            }, 400);
            jQuery('body').addClass('vcht_open');
            jQuery('#vcht_chatframe').addClass('vcht_open');
            jQuery('#vcht_iconMinify').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            
            vcht_recoverChat();
        }
    } else {
        jQuery('#vcht_loader').fadeOut(100);
    }
    jQuery('#chatWrite textarea').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            vcht_sendMessage();
        }
    });
}

function vcht_checkEmail(email) {
    if (email.indexOf("@") != "-1" && email.indexOf(".") != "-1" && email != "")
        return true;
    return false;
}


function vcht_sendEmail() {
    jQuery('#chatEmail_email,#chatEmail_msg').parent().removeClass('has-error');
    var phone = jQuery('#chatEmail_phone').val();
    var msg = jQuery('#chatEmail_msg').val();
    var error = false;

    if (msg.length < 5) {
        error = true;
        jQuery('#chatEmail_msg').parent().addClass('has-error');
    }
    if (!error) {

        jQuery.ajax({
            url: vcht_ajaxurl,
            type: 'post',
            data: {
                action: 'vcht_sendEmail',
                userID: vcht_userID,
                username: vcht_username,
                email: vcht_email,
                phone: phone,
                message: msg
            },
            success: function (repS) {
                jQuery('#emailForm').fadeOut(500);
                setTimeout(function () {
                    jQuery('#emailForm').html('<h4>' + vcht_texts[10] + '</h4>');
                    jQuery('#emailForm').fadeIn(500);
                }, 1000);

                setTimeout(function () {
                    document.location.href = document.location.href;
                }, 3500);

            }
        });
    }
}

function vcht_recoverChat() {
    jQuery.ajax({
        url: vcht_ajaxurl,
        type: 'post',
        data: {
            action: 'vcht_recoverChat',
            logID: vcht_logID
        },
        success: function (repS) {
            var rep = jQuery.parseJSON(repS);
            jQuery('#chatHistory').html('');
            jQuery.each(rep, function (i) {
                var msg = this;
                if (msg.isOperator == '1') {
                    if (i == rep.length - 1) {
                        vcht_operatorSay(msg);
                    } else {
                        vcht_operatorSay(msg, true);
                    }
                } else {
                    vcht_userSay(msg.content);
                }
            });
            if (jQuery('#chatPanel').is('.mob')) {
                vcht_defaultOverflow = jQuery('body').css('overflow-y');
                jQuery('body').css('overflow-y', 'hidden');
                setTimeout(function () {
                    jQuery('#chatHistory').css({
                        height: jQuery('#vcht_chatframe').innerHeight() - (jQuery('#chatHeader').outerHeight() + jQuery('#chatWrite').outerHeight()) - 18
                    });
                    if (jQuery('#chatPanel').is('.mob')) { jQuery('#chatHistory').scrollTop(jQuery('#chatHistory')[0].scrollHeight); }
                    jQuery('#')
                }, 800);
            }else {
                setTimeout(function () {
                    jQuery('#chatContent').scrollTop(jQuery('#chatContent')[0].scrollHeight);
                }, 800);
            }
            jQuery('#chatPanel,#chatHeader').addClass('open');
            vcht_isChatting = true;
            vcht_initTimer();
        }
    });
}

function vcht_validUsername() {
    jQuery('#chatIntro_username').parent().removeClass('has-error');
    jQuery('#chatIntro_email').parent().removeClass('has-error');

    var username = jQuery('#chatIntro_username').val();
    var email = jQuery('#chatIntro_email').val();
    if (username == "") {
        jQuery('#chatIntro_username').parent().addClass('has-error');
    } else if(!vcht_checkEmail(email)){
        jQuery('#chatIntro_email').parent().addClass('has-error');
    }else {
        jQuery('#chatIntro').slideUp(250);
        vcht_username = jQuery('#chatIntro_username').val();
        vcht_email = jQuery('#chatIntro_email').val();
        sessionStorage.vcht_username = vcht_username;
        sessionStorage.vcht_email = vcht_email;

        vcht_startChat();
    }
}

function vcht_startChat() {
    jQuery.ajax({
        url: vcht_ajaxurl,
        type: 'post',
        data: {
            action: 'vcht_startChat',
            userID: vcht_userID,
            username: vcht_username,
            email: vcht_email
        },
        success: function (rep) {
            if (rep == 'nobody') {
                jQuery('#vcht_loader').fadeOut(100);
                jQuery('#vcht_chatframe #emailForm').fadeIn(250);
            } else {
                jQuery('#vcht_loader').fadeOut(200);
                jQuery('#chatRoom').delay(200).fadeIn(250);
                if (vcht_logID == 0) {
                    setTimeout(function () {
                        jQuery("#chatContent").getNiceScroll().resize();
                        setTimeout(function () {
                            jQuery("#chatContent").getNiceScroll().resize();
                        }, 250);
                        vcht_operatorSay({userID: 0, content: vcht_texts[4], domelement: ''});
                    }, 250);
                }
                                   
                
            }
        }
    });

}
function vcht_linkify(inputText) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;
    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

    replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

    return replacedText;
}

function vcht_operatorSay(msg, recover) {
    var bubble = jQuery('<div class="bubble_left palette palette-clouds" data-domelement="' + msg.domelement + '"></div>');
    bubble.append('<div class="bubble_arrow"></div>');
    if (msg.userID == 0 && vcht_settings.chatLogo != "") {
        bubble.append('<div class="avatarImg bubble_photo" style="background-image:none;"><img src="' + vcht_settings.chatLogo + '" alt=""  /></div>');
    } else {     
        bubble.append('<div class="avatarImg bubble_photo" style="background-image:none;">' + msg.avatarOperator + '</div>');
    }

    msg.content = vcht_linkify(msg.content);   
    bubble.append(msg.content);
    var username = '';
    if (msg.username && msg.username != "") {
        username = msg.username;
    }
    //bubble.append('<div class="bubble_meta">' + username + '</div>');
    jQuery('#chatIntro').hide();
    jQuery('#chatRoom').show();
    jQuery('#vcht_chatframe').prop('style', '');
    jQuery('#vcht_chatframe').attr('style', '');
    clearInterval(vcht_timerBounce);
    jQuery(this).addClass('open');
    jQuery(this).parent().addClass('open');
    setTimeout(function () {
        jQuery('.nicescroll-rails').removeClass('hidden');
    }, 400);
    jQuery('body').addClass('vcht_open');
    jQuery('#vcht_chatframe').addClass('vcht_open');
    jQuery('#vcht_iconMinify').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');

    jQuery('#chatHistory').append(bubble);
    bubble.fadeIn(250);
    jQuery('#chatContent').scrollTop(jQuery('#chatContent')[0].scrollHeight);
    if (jQuery('#chatPanel').is('.mob')) { jQuery('#chatHistory').scrollTop(jQuery('#chatHistory')[0].scrollHeight); }
    jQuery("#chatContent").getNiceScroll().resize();
    setTimeout(function () {
        jQuery("#chatContent").getNiceScroll().resize();
    }, 400);
    jQuery('.vcht_selectedDom').removeClass('vcht_selectedDom');
    var chkRedir = false;
    if (!recover) {
        if (msg.url && msg.url != "" && msg.url != document.location.href && sessionStorage.lastRedir != msg.id) {
            sessionStorage.lastRedir = msg.id;
            chkRedir = true;
            document.location.href = msg.url;
        }
        if (!chkRedir) {
            if (msg.domElement && msg.domElement != "") {
                vcht_showElement(msg.domElement);
            } else {
                vcht_selectionMode = false;
                jQuery('.vcht_avatarSel').fadeOut(200);
                setTimeout(function () {
                    jQuery('.vcht_avatarSel').remove();
                }, 500);
            }
        }
    }
}

function vcht_userSay(msg) {
    var bubble = jQuery('<div class="bubble_right palette palette-turquoise"></div>');
    bubble.append(msg);
    bubble.append('<div class="bubble_arrow"></div>');
    if (vcht_userPic) {
        bubble.append('<div class="avatarImg bubble_photo" style="background-image:none;">' + vcht_userPic + '</div>');
    } else {
        bubble.append('<div class="avatarImg bubble_photo"></div>');
    }
    jQuery('#chatHistory').append(bubble);
    var username = '';
    if (msg.username && msg.username != "") {
        username = msg.username;
    } else {
        username = vcht_username;
    }
    //bubble.append('<div class="bubble_meta">' + username + '</div>');
    bubble.fadeIn(250);
    jQuery('#chatContent').scrollTop(jQuery('#chatContent')[0].scrollHeight);
    if (jQuery('#chatPanel').is('.mob')) { jQuery('#chatHistory').scrollTop(jQuery('#chatHistory')[0].scrollHeight); }
    jQuery("#chatContent").getNiceScroll().resize();
    setTimeout(function () {
        jQuery("#chatContent").getNiceScroll().resize();
    }, 400);
}
function vcht_sendMessage() {
    if (isNaN(vcht_operatorID)) {
        vcht_operatorID = 0;
    }
    var message = jQuery('#chatWrite textarea').val();
    jQuery('#chatWrite textarea').parent().removeClass('has-error');
    if (message == "") {
        jQuery('#chatWrite textarea').parent().addClass('has-error');
    } else {
        jQuery('#chatWrite textarea').val('');
        message = message.replace(/\n/g, "<br />");
        message = message.replace(/(<([^>]+)>)/ig, "");
        vcht_userSay(message);
        jQuery.ajax({
            url: vcht_ajaxurl,
            type: 'post',
            data: {
                action: 'vcht_newMessage',
                userID: vcht_userID,
                logID: vcht_logID,
                operatorID: vcht_operatorID,
                username: vcht_username,
                email: vcht_email,
                message: message,
                url: document.location.href
            },
            success: function (repS) {
                var rep = jQuery.parseJSON(repS);
                if (vcht_logID == 0) {
                    vcht_logID = parseInt(rep.logID);
                    sessionStorage.vcht_logID = parseInt(vcht_logID);
                }
                if (vcht_operatorID == 0) {
                    vcht_operatorID = parseInt(rep.operatorID);
                    sessionStorage.vcht_operatorID = vcht_operatorID;
                }
                if (!vcht_isChatting) {
                    vcht_isChatting = true;
                    vcht_initTimer();
                    jQuery.ajax({
                        url: vcht_ajaxurl,
                        type: 'post',
                        data: {
                            action: 'vcht_geo',
                            logID: vcht_logID
                        }
                    });
                } 
            }
        });
    }

}


function vcht_isIframe() {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

function vcht_startSelection() {
    vcht_selectionMode = true;
}

function vcht_showElement(el, avatarImg) {
    if (jQuery(el).length > 0) {
        if (jQuery('.vcht_avatarSel').length > 0) {
            vcht_avatarSel = jQuery('.vcht_avatarSel');
        } else {
            if (avatarImg) {
                vcht_avatarSel = jQuery('<div class="vcht_avatarSel" style="background-image: none;"><div class="vcht_avatarArrow"></div>' + avatarImg + '</div>');
            } else {
                vcht_avatarSel = jQuery('<div class="vcht_avatarSel"><div class="vcht_avatarArrow"></div></div>');
            }
        }
        jQuery('body').append(vcht_avatarSel);
        jQuery(el).addClass('vcht_selectedDom');
        if (vcht_isAnyParentFixed(jQuery(el))) {
            if (jQuery(el).position().top - 140 < 0) {
                vcht_avatarSel.addClass('bottom');
                vcht_avatarSel.css({
                    top: jQuery(el).position().top + jQuery(el).outerHeight() + 80,
                    left: jQuery(el).position().left + jQuery(el).outerWidth() / 2
                });
            } else {
                vcht_avatarSel.removeClass('bottom');
                vcht_avatarSel.css({
                    top: jQuery(el).position().top - 80,
                    left: jQuery(el).position().left + jQuery(el).outerWidth() / 2
                });
            }
            jQuery(vcht_documentBody).animate({scrollTop: jQuery(el).position().top - 200}, 500);
        } else {
            if (jQuery(el).offset().top - 140 < 0) {
                vcht_avatarSel.addClass('bottom');
                vcht_avatarSel.css({
                    top: jQuery(el).offset().top + jQuery(el).outerHeight() + 80,
                    left: jQuery(el).offset().left + jQuery(el).outerWidth() / 2
                });
            } else {
                vcht_avatarSel.removeClass('bottom');
                vcht_avatarSel.css({
                    top: jQuery(el).offset().top - 80,
                    left: jQuery(el).offset().left + jQuery(el).outerWidth() / 2
                });
            }
            jQuery('body').animate({scrollTop: jQuery(el).offset().top - 200}, 500);
        }
    }
}

function vcht_isAnyParentFixed($el, rep) {
    if (!rep) {
        var rep = false;
    }
    try {
        if ($el.parent().length > 0 && $el.parent().css('position') == "fixed") {
            rep = true;
        }
    } catch (e) {
    }
    if (!rep && $el.parent().length > 0) {
        rep = vcht_isAnyParentFixed($el.parent(), rep);
    }
    return rep;
}
