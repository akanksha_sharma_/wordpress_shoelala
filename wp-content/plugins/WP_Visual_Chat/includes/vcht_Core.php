<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of vcht_Core
 *
 * @author Asibun
 */
if (!defined('ABSPATH'))
    exit;

class vcht_Core
{

    /**
     * The single instance
     * @var    object
     * @access  private
     * @since    1.0.0
     */
    private static $_instance = null;

    /**
     * Settings class object
     * @var     object
     * @access  public
     * @since   1.0.0
     */
    public $settings = null;

    /**
     * The version number.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $_version;

    /**
     * The token.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $_token;

    /**
     * The main plugin file.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $file;

    /**
     * The main plugin directory.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $dir;

    /**
     * The plugin assets directory.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $assets_dir;

    /**
     * The plugin assets URL.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $assets_url;

    /**
     * Suffix for Javascripts.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $templates_url;

    /**
     * Suffix for Javascripts.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $script_suffix;

    /**
     * For menu instance
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $menu;

    /**
     * For template
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $plugin_slug;

    /**
     * Constructor function.
     * @access  public
     * @since   1.0.0
     * @return  void
     */
    public function __construct($file = '', $version = '1.0.0')
    {
        $this->_version = $version;
        $this->_token = 'vcht';
        error_reporting(E_ERROR | E_PARSE);
        $this->file = $file;
        $this->dir = dirname($this->file);
        $this->assets_dir = trailingslashit($this->dir) . 'assets';
        $this->assets_url = esc_url(trailingslashit(plugins_url('/assets/', $this->file)));
        $this->templates_url = esc_url(trailingslashit(plugins_url('/templates/', $this->file)));
        add_action('wp_enqueue_scripts', array($this, 'frontend_enqueue_scripts'), 10, 1);
        add_action('wp_enqueue_scripts', array($this, 'frontend_enqueue_styles'), 10, 1);
        add_action('wp_ajax_nopriv_vcht_newMessage', array($this, 'new_message'));
        add_action('wp_ajax_vcht_newMessage', array($this, 'new_message'));
        add_action('wp_ajax_nopriv_vcht_check_user_chat', array($this, 'check_user_chat'));
        add_action('wp_ajax_vcht_check_user_chat', array($this, 'check_user_chat'));
        add_action('wp_ajax_nopriv_vcht_recoverChat', array($this, 'recoverChat'));
        add_action('wp_ajax_vcht_recoverChat', array($this, 'recoverChat'));
        add_action('wp_ajax_nopriv_vcht_startChat', array($this, 'startChat'));
        add_action('wp_ajax_vcht_startChat', array($this, 'startChat'));
        add_action('wp_ajax_nopriv_vcht_sendEmail', array($this, 'sendEmail'));
        add_action('wp_ajax_vcht_sendEmail', array($this, 'sendEmail'));
        add_action('wp_ajax_nopriv_vcht_lifeTimer', array($this, 'lifeTimer'));
        add_action('wp_ajax_vcht_lifeTimer', array($this, 'lifeTimer'));
        add_action('wp_ajax_nopriv_vcht_geo', array($this, 'geolocalize'));
        add_action('wp_ajax_vcht_geo', array($this, 'geolocalize'));   
        add_action('plugins_loaded', array($this, 'init_localization'));
        add_action('wp_head', array($this, 'options_custom_styles'));

    }

    /*
     * Plugin init localization
     */
    public function init_localization()
    {
        $moFiles = scandir(trailingslashit($this->dir) . 'languages/');
        $selFile = '';
        foreach ($moFiles as $moFile) {
            if (strlen($moFile) > 3 && substr($moFile, -3) == '.mo' && strpos(strtolower($moFile), strtolower(get_locale())) > -1){
                $selFile = $moFile;
            load_textdomain('WP_Visual_Chat', trailingslashit($this->dir) . 'languages/' . $moFile);
        }

        }
        if (!$selFile) {
            load_textdomain('WP_Visual_Chat', trailingslashit($this->dir) . 'languages/WP_Visual_Chat_en_US.mo');
        }
    }
    

    // Ajax : geolocalize user
    public function geolocalize(){
        global $wpdb;
        
        $logID = esc_sql($_POST['logID']);
        echo $logID;
        $table_name = $wpdb->prefix . "vcht_logs";
        $rowsC = $wpdb->get_results("SELECT * FROM $table_name  WHERE id=$logID");
        if(count($rowsC)>0){
            $log = $rowsC[0];
            $country = '';
            $city = '';
            $country_info = $this->get_country_city_from_ip($log->ip);
            
            if($country_info['country'] != 'not found'){
                $country = $country_info['country'];                
            }
            if($country_info['town'] != 'not found'){
                $city = $country_info['town'];                
            }
            
            $wpdb->update($table_name, array('country' => $country, 'city' => $city), array('id' => $log->id));
        }
        
        die();
    }
    
    //get city info from IP Adress
    private function get_country_city_from_ip($ip) {
    if (!filter_var($ip, FILTER_VALIDATE_IP)) {
        throw new InvalidArgumentException("IP is not valid");
    }
 
    //contact ip-server
    $response = @file_get_contents('http://www.netip.de/search?query=' . $ip);
    if (empty($response)) {
        return "error";
    }
 
    $patterns = array();
    $patterns["domain"] = '#Name: (.*?)&nbsp;#i';
    $patterns["country"] = '#Country: (.*?)&nbsp;#i';
    $patterns["state"] = '#State/Region: (.*?)<br#i';
    $patterns["town"] = '#City: (.*?)<br#i';
 
    $ipInfo = array();
 
    foreach ($patterns as $key => $pattern) {
        $ipInfo[$key] = preg_match($pattern, $response, $value) && !empty($value[1]) ? $value[1] : 'not found';
    }
 
    return $ipInfo;
}


    /*
     * User life timer
     */
    public function lifeTimer()
    {
        $vcht_id = $_POST['vcht_id'];
        $name = $_POST['name'];

        global $wpdb;
        $userID = get_current_user_id();
        $chkOperator = false;
        if ($userID > 0) {
            $table_name = $wpdb->prefix . "vcht_operators";
            $operators = $wpdb->get_results("SELECT * FROM $table_name WHERE userID=" . $userID . " LIMIT 1");
            if (count($operators) > 0) {
                $chkOperator = true;
            }
            $user = get_userdata($userID);
            $name = $user->display_name;
        }
        if (!$chkOperator) {

            $table_name = $wpdb->prefix . "vcht_users";
            if ($vcht_id > 0) {
                $wpdb->update($table_name, array('lastActivity' => date('Y-m-d h:i:s'), 'username' => $name, 'userID' => $userID), array('id' => $vcht_id));


                $table_name = $wpdb->prefix . "vcht_logs";
                $log = $wpdb->get_results("SELECT * FROM $table_name WHERE vcht_id=" . $vcht_id . " AND finished=0 LIMIT 1");
                if(count($log)>0){
                    $log = $log[0];
                    echo '*'.$log->id;
                }else {
                    echo $vcht_id;
                }

            } else {

                $table_name = $wpdb->prefix . "vcht_users";
                $user = $wpdb->get_results('SELECT * FROM '.$table_name.' WHERE username="'. $name .'" LIMIT 1');
                if (count($user) > 0) {
                    $wpdb->update($table_name, array('lastActivity' => date('Y-m-d h:i:s'), 'username' => $name, 'userID' => $userID), array('id' => $user[0]->id));
                    echo $user[0]->id;
                } else {
                    $wpdb->insert($table_name, array('lastActivity' => date('Y-m-d h:i:s'), 'username' => $name, 'userID' => $userID,'IP'=>$_SERVER['REMOTE_ADDR']));
                    echo $wpdb->insert_id;
                }
            }
        } else {
            echo 'operator';
        }

        die();
    }

    public function getCorrectText($text){
        global $wpdb;
        $settings = $this->getSettings();
        $rep = __($text, 'WP_Visual_Chat');
        if($settings->usePoFile == 0){
        $table_name = $wpdb->prefix . "vcht_texts";       
          if($text == 'Hello'){
            $text = 'Hello :)\nPlease write your question.';
          }
          if($text == 'No operator'){
            $text = 'Sorry, there is currently no operator online.\nIf you wish, send us your question using the form below.';
          }
          if($text == "Thank you.Your message has been sent.We will contact you soon."){
            $text = "Thank you.\nYour message has been sent.\nWe will contact you soon.";
          }   

          $text_db = $wpdb->get_results("SELECT * FROM $table_name WHERE original='".$text."' LIMIT 1");
          if(count($text_db) > 0){
            $rep = $text_db[0]->content;
          }

        } else {            
          if($text == 'No operator'){
            $rep = __('No operator Msg', 'WP_Visual_Chat');              
          }         
          if($text == 'Thank you.Your message has been sent.We will contact you soon.'){
            $rep = __('Message sent', 'WP_Visual_Chat');              
          }     
          
        }
        return $rep;
    }


    /*
     * Get a text by Index
     */
    public function getText($textIndex)
    {
        $rep = "";
        switch ($textIndex) {
            case 1:
                $rep = $this->getCorrectText('Need Help ?', 'WP_Visual_Chat');
                break;
            case 2:
                $rep = $this->getCorrectText('Enter your name', 'WP_Visual_Chat');
                break;
            case 3:
                $rep = $this->getCorrectText('Start', 'WP_Visual_Chat');
                break;
            case 4:
                $rep = $this->getCorrectText('Hello', 'WP_Visual_Chat');
                break;
            case 5:
                $rep = $this->getCorrectText('This discussion is finished.', 'WP_Visual_Chat');
                break;
            case 6:
                $rep = $this->getCorrectText('No operator', 'WP_Visual_Chat');
                break;
            case 7:
                $rep = $this->getCorrectText('Enter your email here', 'WP_Visual_Chat');
                break;
            case 8:
                $rep = $this->getCorrectText('Write your message here', 'WP_Visual_Chat');
                break;
            case 9:
                $rep = $this->getCorrectText('Send this message', 'WP_Visual_Chat');
                break;
            case 10:
                $rep = $this->getCorrectText("Thank you.Your message has been sent.We will contact you soon.", 'WP_Visual_Chat');
                break;
            case 11:
                $rep = $this->getCorrectText('Phone number', 'WP_Visual_Chat');
                break;
            case 12:
                $rep = $this->getCorrectText('Username', 'WP_Visual_Chat');
                break;
            case 13:
                $rep = $this->getCorrectText('Email', 'WP_Visual_Chat');
                break;
        }

        $rep = str_replace("\n", "<br/>", $rep);
        $rep = str_replace('\n', "<br/>", $rep);
        return $rep;
    }

    /**
     * Checks if the template is assigned to the page
     *
     * @version    1.0.0
     * @since    1.0.0
     */
    public function view_project_template($template)
    {

        global $post;

        if (!isset($this->templates[get_post_meta($post->ID, '_vcht_template', true)])) {
            return $template;
        }

        $file = plugin_dir_path(__FILE__) . '../templates/' . get_post_meta($post->ID, '_vcht_template', true);
        if (file_exists($file)) {
            return $file;
        }
        return $template;
    }


    /*
     * Chat custom styles integration
     */
    public function options_custom_styles()
    {
        $output = '';
        $settings = $this->getSettings();

        if(substr($settings->colorA,0,1) != '#'){
          $settings->colorA = '#'.$settings->colorA;
        }
        if(substr($settings->colorB,0,1) != '#'){
          $settings->colorB = '#'.$settings->colorB;
        }
        if(substr($settings->colorC,0,1) != '#'){
          $settings->colorC = '#'.$settings->colorC;
        }
        if(substr($settings->colorD,0,1) != '#'){
          $settings->colorD = '#'.$settings->colorD;
        }
        if(substr($settings->colorA,0,1) != '#'){
          $settings->colorE = '#'.$settings->colorE;
        }
        if(substr($settings->colorA,0,1) != '#'){
          $settings->colorF = '#'.$settings->colorF;
        }
        if(substr($settings->colorA,0,1) != '#'){
          $settings->colorG = '#'.$settings->colorG;
        }
        if(substr($settings->shineColor,0,1) != '#'){
          $settings->shineColor = '#'.$settings->shineColor;
        }

        $output .= '.vcht_selectedDom  {';
        $output .= ' -moz-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' !important;';
        $output .= ' -webkit-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' !important;';
        $output .= ' -o-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' !important;';
        $output .= ' box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' !important;';
        $output .= '}';
        $output .= "\n";
        $output .= '@-o-keyframes glow {';
        $output .= '0% { -o-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';} 50% { -o-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . '; } 100% { -o-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';} ';
        $output .= '}';
        $output .= '@-moz-keyframes glow {';
        $output .= '0% { -moz-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';} 50% { -moz-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . '; } 100% { -moz-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';} ';
        $output .= '}';
        $output .= '@-webkit-keyframes glow {';
        $output .= '0% { -webkit-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';} 50% { -webkit-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . '; } 100% { -webkit-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';} ';
        $output .= '}';
        $output .= '@keyframes glow {';
        $output .= '0% { box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';} 50% { box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . '; } 100% { box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';} ';
        $output .= '}';
        $output .= "\n";

        if ($output != '') {
            $output = "\n<style id=\"vcht_frontend_styles\" >\n" . $output . "</style>\n";
            echo $output;
        }

        $output = '';
  //      $settings = $this->getSettings();

        $output .= '#vcht_chatframe {';
        $output .= ' color:' . $settings->colorB . '; ';
        $output .= '}';
        $output .= "\n";
        $output .= '#vcht_chatframe .vcht_chat .palette-turquoise,.btn-primary,.btn-primary:hover,.btn-primary:active {';
        $output .= ' background-color:' . $settings->colorA . '; ';
        $output .= ' color:' . $settings->colorE . '; ';
        $output .= '}';
        $output .= "\n";
        $output .= '#vcht_chatframe .vcht_chat .bubble_right .bubble_arrow {';
        $output .= ' border-color: transparent transparent transparent ' . $settings->colorA . '; ';
        $output .= '}';
        $output .= "\n";
        $output .= '#vcht_chatframe .vcht_chat .palette-clouds {';
        $output .= ' background-color:' . $settings->colorC . '; ';
        $output .= ' color:' . $settings->colorF . '; ';
        $output .= '}';
        $output .= "\n";
        $output .= '#vcht_chatframe .vcht_chat .form-control:focus {';
        $output .= ' border-color:' . $settings->colorA . '; ';
        $output .= '}';
        $output .= "\n";
        $output .= '#vcht_chatframe .vcht_chat .anim {';
        $output .= ' background: none repeat scroll 0 0 ' . $settings->colorA . '; ';
        $output .= '}';
        $output .= '#vcht_chatframe .vcht_chat .btn-default,.btn-default:hover,.btn-default:active  {';
        $output .= ' background-color:' . $settings->colorD . '; ';
        $output .= '}';
        $output .= "\n";
        $output .= '#vcht_chatframe .vcht_chat .btn-primary,.btn-primary:hover,.btn-primary:active  {';
        $output .= ' background-color:' . $settings->colorG . '; ';
        $output .= '}';
        $output .= "\n";
        $output .= '#vcht_chatframe .vcht_chat .form-control {';
        $output .= ' border-color:' . $settings->colorD . '; ';
        $output .= '}';
        $output .= "\n";

        $output .= '#vcht_chatframe .vcht_chat .bubble_left .bubble_arrow {';
        $output .= ' border-color: transparent ' . $settings->colorC . ' transparent transparent; ';
        $output .= '}';
        $output .= "\n";
        $output .= '#vcht_chatframe .vcht_selectedDom {';
        $output .= ' -moz-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' ;';
        $output .= ' -webkit-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' ;';
        $output .= ' -o-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' ;';
        $output .= ' -box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' ;';
        $output .= '}';
        $output .= "\n";
        $output .= '#vcht_chatframe .nicescroll-rails > div {';
        $output .= ' background-color:' . $settings->colorD . '; ';
        $output .= '}';
        $output .= "\n";
        $output .= '#vcht_chatframe .avatarImg {';
        $output .= '  background-image: url(' . $settings->chatDefaultPic . ');';
        $output .= '}';
        $output .= "\n";
        $output .= '@-o-keyframes glow {';
        $output .= '0% { -o-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';} 50% { -o-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . '; } 100% { -o-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';} ';
        $output .= '}';
        $output .= '@-moz-keyframes glow {';
        $output .= '0% { -moz-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';} 50% { -moz-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . '; } 100% { -moz-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';} ';
        $output .= '}';
        $output .= '@-webkit-keyframes glow {';
        $output .= '0% { -webkit-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';} 50% { -webkit-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . '; } 100% { -webkit-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';} ';
        $output .= '}';
        $output .= '@keyframes glow {';
        $output .= '0% { box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';} 50% { box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . '; } 100% { box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';} ';
        $output .= '}';
        $output .= "\n";

        if ($output != '') {
            $output = "\n<style id=\"vcht_styles\" >\n" . $output . "</style>\n";
            echo $output;
        }
    }

    /**
     * Load frontend CSS.
     * @access public
     * @since 1.0.0
     * @return void
     */
    public function frontend_enqueue_styles($hook = '')
    {
        global $wp_styles;
        wp_register_style($this->_token . '-bootstrap', esc_url($this->assets_url) . 'bootstrap/css/bootstrap.min.css', array(), $this->_version);
        wp_enqueue_style($this->_token . '-bootstrap');
        wp_register_style($this->_token . '-flat-ui', esc_url($this->assets_url) . 'css/flat-ui.min.css', array(), $this->_version);
        wp_enqueue_style($this->_token . '-flat-ui');
        wp_register_style($this->_token . '-chat', esc_url($this->assets_url) . 'css/chat.min.css', array(), $this->_version);
        wp_enqueue_style($this->_token . '-chat');
        wp_register_style($this->_token . '-ie10', esc_url($this->assets_url) . 'css/frontend_ie9.min.css', array(), $this->_version);
        wp_enqueue_style($this->_token . '-ie10');
        $wp_styles->add_data($this->_token . '-ie10', 'conditional', 'lt IE 10');
    }

    /**
     * Load frontend Javascript.
     * @access  public
     * @since   1.0.0
     * @return void
     */
    public function frontend_enqueue_scripts($hook = '')
    {
        $settings = $this->getSettings();
    /*    wp_register_script($this->_token . '-jquery-ui', esc_url($this->assets_url) . 'js/jquery-ui-1.10.3.custom.min.js', array('jquery'), $this->_version);
        wp_enqueue_script($this->_token . '-jquery-ui');*/
        wp_register_script($this->_token . '-jquery-nicescroll', esc_url($this->assets_url) . 'js/jquery.nicescroll.min.js', array('jquery'), $this->_version);
        wp_enqueue_script($this->_token . '-jquery-nicescroll');
        wp_register_script($this->_token . '-jquery-browser', esc_url($this->assets_url) . 'js/jquery.browser.min.js', array('jquery'), $this->_version);
        wp_enqueue_script($this->_token . '-jquery-browser');
        wp_register_script($this->_token . '-jquery-cookie', esc_url($this->assets_url) . 'js/jquery.cookie.min.js', array('jquery'), $this->_version);
        wp_enqueue_script($this->_token . '-jquery-cookie');

        wp_register_script($this->_token . '-chat', esc_url($this->assets_url) . 'js/chat.min.js', array('jquery','jquery-ui-core','jquery-ui-position'), $this->_version);
        wp_enqueue_script($this->_token . '-chat');
        $settings = $this->getSettings();
        $settings->purchaseCode = "";
        wp_localize_script($this->_token . '-chat', 'vcht_position', $settings->chatPosition);
        wp_localize_script($this->_token . '-chat', 'vcht_url', get_page_link($settings->pageID));
        if (isset($_GET['vcht_element'])) {
            global $wpdb;
            $table_name = $wpdb->prefix . "vcht_messages";
            $msg = $wpdb->get_results("SELECT * FROM $table_name WHERE id=" . $_GET['vcht_element'] . " LIMIT 1");
            wp_localize_script($this->_token . '-chat', 'vcht_elementShow', $msg[0]->domElement);

        } else {
            wp_localize_script($this->_token . '-chat', 'vcht_elementShow', "");
        }
        wp_localize_script($this->_token . '-chat', 'vcht_settings', array("enableInitiate"=>$settings->enableInitiate,"chatLogo"=>$settings->chatLogo,"bounceFx"=>$settings->bounceFx));

       // setcookie('vchta',admin_url('admin-ajax.php'));
        wp_localize_script($this->_token . '-chat', 'vcht_ajaxurl', array(admin_url('admin-ajax.php')));
        $user = false;
        $userPic = "";
        $userID = 0;
        $userEmail = '';
        if (is_user_logged_in()) {
            $current_user = wp_get_current_user();
            $userID = $current_user->ID;
            $user = $current_user->display_name;
            if (!$user || $user == "") {
                $user = $current_user->user_login;
            }
            $userPic = get_avatar($userID, 48);
            $userEmail = $current_user->user_email;
        }
        wp_localize_script($this->_token . '-chat', 'vcht_userID', array($userID));
        wp_localize_script($this->_token . '-chat', 'vcht_user', array($user));
        wp_localize_script($this->_token . '-chat', 'vcht_userPic', array($userPic));
        wp_localize_script($this->_token . '-chat', 'vcht_userEmail', array($userEmail));


        $textsJS = array();
        $textsJS[] = '';
        for ($i = 1; $i <= 11; $i++) {
            $textsJS[] = $this->getText($i);
        }
        wp_localize_script($this->_token . '-chat', 'vcht_texts', $textsJS);
    }

    /**
     * Return settings.
     * @access  public
     * @since   1.0.0
     * @return  void
     */
    public function getSettings()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "vcht_settings";
        $settings = $wpdb->get_results("SELECT * FROM $table_name WHERE id=1 LIMIT 1");
        if ($settings[0]) {
            return $settings[0];
        } else {
            return false;
        }
    }

    /**
     * Return texts.
     * @access  public
     * @since   1.0.0
     * @return  void
     */
    public function getTexts()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "vcht_texts";
        $texts = array();
        $rows = $wpdb->get_results("SELECT * FROM $table_name ORDER BY id ASC");
        $emptyObj = new StdClass();
        $emptyObj->content = ' ';
        $texts[] = $emptyObj;
        foreach ($rows as $value) {
            $value->content = nl2br($value->content);
            $texts[] = $value;
        }
        return $texts;
    }

    /*
     * ajax : return chat history
     */

    public function recoverChat()
    {
        global $wpdb;
        $logID = esc_sql($_POST['logID']);
        $rowsC = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_logs WHERE id=$logID");
        $messages = array();
        $rows = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_messages WHERE logID=$logID ORDER BY id ASC");
        foreach ($rows as $value) {
            if ($value->isOperator) {
                $value->avatarOperator = get_avatar($value->userID, 37);
                $user = get_userdata($value->userID);
                $username = $user->display_name;
                if (!$username || $username == "") {
                    $username = $user->user_login;
                }
                $value->username = $username;
            } else {
                $value->username = $rowsC[0]->username;
            }
            $value->content = stripslashes(nl2br($value->content));
            $messages[] = $value;
        }
        echo json_encode($messages);
        die();
    }

    /*
     * Ajax : user start Chat
     */

    public function startChat()
    {

        global $wpdb;
        $chkOperator = false;
        $rowsO = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_operators");
        foreach ($rowsO as $value) {
            if (abs(strtotime(date('Y-m-d h:i:s')) - strtotime($value->lastActivity)) < 10) {
                $chkOperator = true;
            }
        }
        if ($chkOperator) {
            echo '1';
        } else {
            echo 'nobody';
        }
        die();
    }

    /*
     * Ajax: send email
     */

    public function sendEmail()
    {
        $settings = $this->getSettings();
        $userID = esc_sql($_POST['userID']);
        $username = esc_sql($_POST['username']);
        $email = esc_sql($_POST['email']);
        $msg = esc_sql($_POST['message']);
        $phone = esc_sql($_POST['phone']);

        if (is_user_logged_in()){
            global $current_user;
            get_currentuserinfo();
            $email = $current_user->user_email;
            $username =  $current_user->display_name;
        }

        $headers = "Return-Path: " . $email . "\n";
        $headers .= "From:" . $email . "\n";
        $headers .= "X-Mailer: PHP " . phpversion() . "\n";
        $headers .= "Reply-To: " . $email . "\n";
        $headers .= "X-Priority: 3 (Normal)\n";
        $headers .= "Mime-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=utf-8\n";
        $content = '<p>'.$this->getText(12).' : <strong>'.$username.'</strong>';
        $content .= '<p>'.$this->getText(11).' : <strong>'.$phone.'</strong>';
        $content .= '<p>'.$this->getText(13).' : <strong>'.$email.'</strong>';
        $content .= '<p>' . nl2br(stripslashes($msg)) . '</p>';

        wp_mail($settings->adminEmail, $settings->emailSubject, $content, $headers);
        die();
    }

    /*
     * Ajax : check new messages
     */

    public function check_user_chat()
    {
        $logID = esc_sql($_POST['logID']);
        global $wpdb;
        $date_past = date('Y-m-d H:i:s', time() - 1 * 60);
        $rep = array();
        $rep['messages'] = array();
        $rowsC = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_logs WHERE id=$logID");
        $rep['finished'] = $rowsC[0]->finished;

        if ($rowsC[0]->operatorID > 0) {
            if (abs(strtotime(date('Y-m-d h:i:s')) - strtotime($rowsC[0]->operatorLastActivity)) > 25) {
                $wpdb->update($wpdb->prefix . "vcht_logs", array('finished' => true), array('id' => $logID));
                $rep['finished'] = 1;
            }
            $rowsO = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_operators WHERE userID=" . $rowsC[0]->operatorID . " LIMIT 1");
            if (count($rowsO) == 0) {
                $wpdb->update($wpdb->prefix . "vcht_logs", array('finished' => true), array('id' => $logID));
                $rep['finished'] = 1;
            }
        } else {
            $rowsO = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_operators");
            if (count($rowsO) == 0) {
                $wpdb->update($wpdb->prefix . "vcht_logs", array('finished' => true), array('id' => $logID));
                $rep['finished'] = 1;
            }
        }

        $rows = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_messages WHERE logID=$logID  AND isOperator=1 AND isRead=0");
        foreach ($rows as $value) {
            if ($value->isOperator) {
                $value->avatarOperator = get_avatar($value->userID, 37);
                $user = get_userdata($value->userID);
                $username = $user->display_name;
                if (!$username || $username == "") {
                    $username = $user->user_login;
                }
                $value->username = $username;
            }
            $value->content = stripslashes(nl2br($value->content));
            $rep['messages'][] = $value;
            $wpdb->update($wpdb->prefix . "vcht_messages", array('isRead' => true), array('id' => $value->id));
        }
        $wpdb->update($wpdb->prefix . "vcht_logs", array('lastActivity' => date('Y-m-d h:i:s')), array('id' => $logID));
        echo json_encode($rep);
        die();
    }

    /*
     * Receive new message from user
     */

    public function new_message()
    {
        global $wpdb;
        $userID = $_POST['userID'];
        $operatorID = $_POST['operatorID'];
        $username = esc_sql($_POST['username']);
        $email = esc_sql($_POST['email']);
        $message = esc_sql($_POST['message']);
        $message = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $message);
        $logID = $_POST['logID'];
        $operatorID = $_POST['operatorID'];
        $url = ($_POST['url']);


        if ($logID == 0) {
            if ($userID > 0) {
                $user = get_userdata($userID);
                $username = $user->display_name;
                if (!$username || $username == "") {
                    $username = $user->user_login;
                }
                $rows = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_logs WHERE finished=0 AND operatorID=$operatorID AND userID=$userID  LIMIT 1");
            } else {
                $rows = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_logs  WHERE finished=0 AND  operatorID=$operatorID AND username='$username'  LIMIT 1");
            }
            if (count($rows) > 0) {
                $logID = $rows[0]->id;
                $operatorID = $rows[0]->operatorID;
            } else {
                $rows_affected = $wpdb->insert($wpdb->prefix . "vcht_logs", array('userID' => $userID, 'username' => $username, 'operatorID' => $operatorID,'email'=>$email, 'date' => date('Y-m-d h:i:s'), 'ip' => $_SERVER['REMOTE_ADDR']));
                $logID = $wpdb->insert_id;
            }
        }
        $wpdb->update($wpdb->prefix . "vcht_logs", array('lastActivity' => date('Y-m-d h:i:s')), array('id' => $logID));

        $rows_affected = $wpdb->insert($wpdb->prefix . "vcht_messages", array('logID' => $logID, 'content' => stripslashes($message), 'date' => date('Y-m-d h:i:s'), 'url' => $url));
        echo '{"logID": "' . $logID . '", "operatorID": "' . $operatorID . '"}';
        die();
    }

    /**
     * Main Instance
     *
     *
     * @since 1.0.0
     * @static
     * @return Main instance
     */
    public static function instance($file = '', $version = '1.0.0')
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self($file, $version);
        }
        return self::$_instance;
    }

// End instance()

    /**
     * Cloning is forbidden.
     *
     * @since 1.0.0
     */
    public function __clone()
    {
        _doing_it_wrong(__FUNCTION__, $this->getCorrectText(''), $this->_version);
    }

// End __clone()

    /**
     * Unserializing instances of this class is forbidden.
     *
     * @since 1.0.0
     */
    public function __wakeup()
    {
        _doing_it_wrong(__FUNCTION__, $this->getCorrectText('Cheatin&#8217; huh?'), $this->_version);
    }

// End __wakeup()

    /**
     * Log the plugin version number.
     * @access  public
     * @since   1.0.0
     * @return  void
     */
    private function _log_version_number()
    {
        update_option($this->_token . '_version', $this->_version);
    }

}
