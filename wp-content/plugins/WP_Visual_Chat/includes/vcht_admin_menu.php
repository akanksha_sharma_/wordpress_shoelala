<?php
if (!defined('ABSPATH'))
    exit;

class vcht_admin_menu
{

    /**
     * The single instance
     * @var    object
     * @access  private
     * @since    1.0.0
     */
    private static $_instance = null;

    /**
     * The main plugin object.
     * @var    object
     * @access  public
     * @since    1.0.0
     */
    public $parent = null;

    /**
     * Prefix for plugin settings.
     * @var     string
     * @access  publicexport
     *
     * @since   1.0.0
     */
    public $base = '';

    /**
     * Available settings for plugin.
     * @var     array
     * @access  public
     * @since   1.0.0
     */
    public $settings = array();

    /**
     * The version number.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $_version;

    /**
     * The token.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $_token;

    /**
     * The main plugin file.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $file;

    /**
     * The main plugin directory.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $dir;

    /**
     * The plugin assets directory.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $assets_dir;

    /**
     * The plugin assets URL.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $assets_url;

    /**
     * Suffix for Javascripts.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $templates_url;

    public function __construct($parent)
    {
        $this->_token = 'vcht';
        $this->parent = $parent;
        $this->dir = dirname($parent->file);
        $this->assets_dir = trailingslashit($this->dir) . 'assets';
        $this->assets_url = esc_url(trailingslashit(plugins_url('/assets/', $parent->file)));

        add_action('admin_menu', array($this, 'add_menu_item'));
        add_action('admin_init', array($this, 'checkRoles'));   
        add_action('admin_init', array($this, 'checkAutomaticUpdates'));    
        add_action('wp_ajax_nopriv_vcht_settings_save', array($this, 'settings_save'));
        add_action('wp_ajax_vcht_settings_save', array($this, 'settings_save'));
        add_action('wp_ajax_nopriv_vcht_texts_save', array($this, 'texts_save'));
        add_action('wp_ajax_vcht_texts_save', array($this, 'texts_save'));
        add_action('wp_ajax_nopriv_vcht_check_operator_chat', array($this, 'check_operator_chat'));
        add_action('wp_ajax_vcht_check_operator_chat', array($this, 'check_operator_chat'));
        add_action('wp_ajax_nopriv_vcht_getLogChat', array($this, 'getLogChat'));
        add_action('wp_ajax_vcht_getLogChat', array($this, 'getLogChat'));
        add_action('wp_ajax_nopriv_vcht_acceptChat', array($this, 'acceptChat'));
        add_action('wp_ajax_vcht_acceptChat', array($this, 'acceptChat'));
        add_action('wp_ajax_nopriv_vcht_operatorSay', array($this, 'operatorSay'));
        add_action('wp_ajax_vcht_operatorSay', array($this, 'operatorSay'));
        add_action('wp_ajax_nopriv_vcht_recoverChats', array($this, 'recoverChats'));
        add_action('wp_ajax_vcht_recoverChats', array($this, 'recoverChats'));
        add_action('wp_ajax_nopriv_vcht_closeChat', array($this, 'closeChat'));
        add_action('wp_ajax_vcht_closeChat', array($this, 'closeChat'));
        add_action('wp_ajax_nopriv_vcht_operatorConnect', array($this, 'operatorConnect'));
        add_action('wp_ajax_vcht_operatorConnect', array($this, 'operatorConnect'));
        add_action('wp_ajax_nopriv_vcht_operatorDisconnect', array($this, 'operatorDisconnect'));
        add_action('wp_ajax_vcht_operatorDisconnect', array($this, 'operatorDisconnect'));
        add_action('wp_ajax_nopriv_vcht_transferChat', array($this, 'transferChat'));
        add_action('wp_ajax_vcht_transferChat', array($this, 'transferChat'));        
        add_action('wp_ajax_nopriv_vcht_checkLicense', array($this, 'checkLicense'));
        add_action('wp_ajax_vcht_checkLicense', array($this, 'checkLicense'));
        add_action('wp_ajax_nopriv_vcht_initiateChat', array($this, 'initiateChat'));
        add_action('wp_ajax_vcht_initiateChat', array($this, 'initiateChat'));
        add_action('wp_ajax_nopriv_vcht_editSentence', array($this, 'editSentence'));
        add_action('wp_ajax_vcht_editSentence', array($this, 'editSentence'));
        add_action('wp_ajax_nopriv_vcht_saveSentence', array($this, 'saveSentence'));
        add_action('wp_ajax_vcht_saveSentence', array($this, 'saveSentence')); 
        add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'), 10, 1);
        add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_styles'), 10, 1);
        add_action('plugins_loaded', array($this, 'init_localization'));
    }

    /*
     * Check roles capabilities
     */
    public function checkRoles()
    {
        global $wp_roles;
        $settings = $this->getSettings();
        $rolesNew = explode(',', $settings->rolesAllowed);
        foreach ($wp_roles->roles as $role) {
            if (strtolower($role['name']) != 'administrator') {
                $name = strtolower($role['name']);
                $name = str_replace(" ", "_", $name);
                if (in_array(($role['name']), $rolesNew) || $role['name'] == "Chat Operator") {
					             $wp_roles->add_cap($name, 'visual_chat');
                } else {
                    $wp_roles->remove_cap($name, 'visual_chat');
                }
            }
        }
    }

    /**
     * Add menu to admin
     * @return void
     */
    public function add_menu_item()
    {
        add_menu_page(__('Visual Chat', 'WP_Visual_Chat'), 'Visual Chat', 'visual_chat', 'vcht-console', array($this, 'submenu_console'), 'dashicons-format-chat');
        add_submenu_page('vcht-console', __('Logs', 'WP_Visual_Chat'), 'Logs', 'visual_chat', 'vcht-logsList', array($this, 'submenu_logsList'));
        add_submenu_page('vcht-console', __('Settings', 'WP_Visual_Chat'), 'Settings', 'manage_options', 'vcht-settings', array($this, 'submenu_settings'));
        add_submenu_page('vcht-console', __('Import', 'WP_Visual_Chat'), 'Import', 'manage_options', 'vcht-import', array($this, 'submenu_import'));
        add_submenu_page('vcht-console', __('Export', 'WP_Visual_Chat'), 'Export', 'manage_options', 'vcht-export', array($this, 'submenu_export'));
    }
    
    /*
     * Check for  updates
     */
     function checkAutomaticUpdates(){
       $settings = $this->getSettings();
       if($settings && $settings->purchaseCode != ""){
         require_once('plugin_update_check.php');      
        $updateChecker = new PluginUpdateChecker_2_0 (
           'https://kernl.us/api/v1/updates/56b8bc85201012a97c245f16/',
           $this->parent->file,
           'vcht',
           1
        );
        $updateChecker->purchaseCode = $settings->purchaseCode;
       }
     }

    /**
     * Load admin CSS.
     * @access  public
     * @since   1.0.0
     * @return void
     */
    public function admin_enqueue_styles($hook = '')
    {
        if (isset($_GET['page']) && substr($_GET['page'],0,5) == "vcht-") {
            wp_register_style($this->_token . '-admin-bootstrap', esc_url($this->assets_url) . 'bootstrap/css/admin-bootstrap.min.css', array(), $this->_version);
            wp_enqueue_style($this->_token . '-admin-bootstrap');
            wp_register_style($this->_token . '-admin-flat-ui', esc_url($this->assets_url) . 'css/admin-flat-ui.min.css', array(), $this->_version);
            wp_enqueue_style($this->_token . '-admin-flat-ui');
            wp_register_style($this->_token . '-admin', esc_url($this->assets_url) . 'css/admin.min.css', array(), $this->_version);
            wp_enqueue_style($this->_token . '-admin');
        }
        if (isset($_GET['page']) && strrpos($_GET['page'], 'vcht-settings') !== false) {
            wp_register_style($this->_token . '-colpick', esc_url($this->assets_url) . 'css/colpick.min.css', array(), $this->_version);
            wp_enqueue_style($this->_token . '-colpick');
        }
        if (isset($_GET['page']) && $_GET['page'] == 'vcht-settings') {
            wp_enqueue_style('thickbox');
        }
    }

// End admin_enqueue_styles()

    /*
     * Check plugin state
     */
    public function checkUpdates()
    {
        if (strpos(get_admin_url(), '?page=vcht-settings') === false && strpos(get_admin_url(), '?page=vcht') !== false && !$this->isUpdated()) {
            wp_redirect(admin_url('admin.php?page=vcht-settings'));
        }
        if (!isset($_COOKIE['pll_updateC']) || $_COOKIE['pll_updateC'] == '0') {
            $this->form_checkUpdates();
        }
    }


    /*
     * Ajax: operator initiate chat
     */
    public function initiateChat(){
        global $wpdb;
        $current_user = wp_get_current_user();
        $operatorID = $current_user->ID;
        $operator = $current_user->display_name;
        if (!$operator || $operator == "") {
            $operator = $current_user->user_login;
        }
        $rows = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_users WHERE id=".$_POST['vcht_id']);
        $user = $rows[0];

        $rows_affected = $wpdb->insert($wpdb->prefix . "vcht_logs", array('lastActivity'=>date('Y-m-d h:i:s'),'userID' => $user->userID,'vcht_id'=>$_POST['vcht_id'], 'username' => $user->username, 'operatorID' => $operatorID, 'date' => date('Y-m-d h:i:s'), 'ip' => $user->IP));
        echo $wpdb->insert_id;
        die();
    }


    /*
     * Plugin init localization
     */
    public function init_localization()
    {
        $moFiles = scandir(trailingslashit($this->dir) . 'languages/');
        foreach ($moFiles as $moFile) {
            if (strlen($moFile) > 3 && substr($moFile, -3) == '.mo' && strpos($moFile, get_locale()) > -1) {
                load_textdomain('WP_Visual_Chat', trailingslashit($this->dir) . 'languages/' . $moFile);
            }
        }
    }

    /**
     * Load admin Javascript.
     * @access  public
     * @since   1.0.0
     * @return void
     */
    public function admin_enqueue_scripts($hook = '')
    {
        if (isset($_GET['page']) && $_GET['page'] == 'vcht-console') {
            global $wpdb;
            wp_register_script($this->_token . '-jquery-ui', esc_url($this->assets_url) . 'js/jquery-ui-1.10.3.custom.min.js', array('jquery'), $this->_version);
            wp_enqueue_script($this->_token . '-jquery-ui');
            wp_register_script($this->_token . '-jquery-touch-punch', esc_url($this->assets_url) . 'js/jquery.ui.touch-punch.min.js', array('jquery'), $this->_version);
            wp_enqueue_script($this->_token . '-jquery-touch-punch');
            wp_register_script($this->_token . '-bootstrap', esc_url($this->assets_url) . 'js/bootstrap.min.js', array('jquery'), $this->_version);
            wp_enqueue_script($this->_token . '-bootstrap');
            wp_register_script($this->_token . '-bootstrap-select', esc_url($this->assets_url) . 'js/bootstrap-select.js', array(), $this->_version);
            wp_enqueue_script($this->_token . '-bootstrap-select');
            wp_register_script($this->_token . '-bootstrap-switch', esc_url($this->assets_url) . 'js/bootstrap-switch.js', array('jquery'), $this->_version);
            wp_enqueue_script($this->_token . '-bootstrap-switch');
            wp_register_script($this->_token . '-flatui-checkbox', esc_url($this->assets_url) . 'js/flatui-checkbox.js', array(), $this->_version);
            wp_enqueue_script($this->_token . '-flatui-checkbox');
            wp_register_script($this->_token . '-flatui-radio', esc_url($this->assets_url) . 'js/flatui-radio.js', array(), $this->_version);
            wp_enqueue_script($this->_token . '-flatui-radio');
            wp_register_script($this->_token . '-jquery-tagsinput', esc_url($this->assets_url) . 'js/jquery.tagsinput.js', array(), $this->_version);
            wp_enqueue_script($this->_token . '-jquery-tagsinput');
            wp_register_script($this->_token . '-jquery-placeholder', esc_url($this->assets_url) . 'js/jquery.placeholder.js', array(), $this->_version);
            wp_enqueue_script($this->_token . '-jquery-placeholder');
            wp_register_script($this->_token . '-mousetrap', esc_url($this->assets_url) . 'js/mousetrap.min.js', 'jquery', $this->_version);
            wp_enqueue_script($this->_token . '-mousetrap');
            wp_register_script($this->_token . '-admin', esc_url($this->assets_url) . 'js/admin.min.js', 'jquery', $this->_version);
            wp_enqueue_script($this->_token . '-admin');
            $user = false;
            $avatarOperator = '';
            $userID = 0;
            if (is_user_logged_in()) {
                $current_user = wp_get_current_user();
                $userID = $current_user->ID;
                $user = $current_user->display_name;
                if (!$user || $user == "") {
                    $user = $current_user->user_login;
                }
                $avatarOperator = get_avatar($userID, 37);
            }
            wp_localize_script($this->_token . '-admin', 'vcht_operatorID', array($userID));
            wp_localize_script($this->_token . '-admin', 'vcht_operator', array($user));
            wp_localize_script($this->_token . '-admin', 'vcht_operatorAvatar', array($avatarOperator));
            wp_localize_script($this->_token . '-admin', 'vcht_siteUrl', array(get_site_url()));

            $sentences = array();
            $sentences = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_sentences ORDER BY title ASC");
            wp_localize_script($this->_token . '-admin', 'vcht_sentences', $sentences);

            $textsJs = array('Transfer chat' => __('Transfer chat', 'WP_Visual_Chat'),
                'There is no chat currently' => __('There is no chat currently', 'WP_Visual_Chat'),
                'Stop chat' => __('Stop chat', 'WP_Visual_Chat'),
                'User disconnects.' => __('User disconnects.', 'WP_Visual_Chat'),
                'This discussion was transferred to you' => __('This discussion was transferred to you', 'WP_Visual_Chat'),
                'Online operators' => __('Online operators', 'WP_Visual_Chat'),
                'Accept this chat' => __('Accept this chat', 'WP_Visual_Chat'),
                'See user logs' => __('See user logs', 'WP_Visual_Chat'),
                'Choose operator' => __('Choose operator', 'WP_Visual_Chat'),
                'No operator' => __('No operator', 'WP_Visual_Chat'),
                'Write your message here' => __('Write your message here', 'WP_Visual_Chat'),
                'Select an element' => __('Select an element', 'WP_Visual_Chat'),
                'Element selected' => __('Element selected', 'WP_Visual_Chat'),
                'No' => __('No', 'WP_Visual_Chat'),
                'Yes' => __('Yes', 'WP_Visual_Chat'),
                'Cancel' => __('Cancel', 'WP_Visual_Chat'),
                'Navigate to the desired page and click on "Select an element" button.' => __('Navigate to the desired page and click on "Select an element" button.', 'WP_Visual_Chat'),
                'Click on the desired item' => __('Click on the desired item', 'WP_Visual_Chat'),
                'Do you want to show this item to the user ?' => __('Do you want to show this item to the user ?', 'WP_Visual_Chat'),
                'ON' => __('ON', 'WP_Visual_Chat'),
                'OFF' => __('OFF', 'WP_Visual_Chat'),
                'Sentences'=>__('Sentences', 'WP_Visual_Chat'),
            );
            wp_localize_script($this->_token . '-admin', 'vcht_textsAdmin', $textsJs);
        } else if (isset($_GET['page']) && substr($_GET['page'],0,5) == "vcht-") {
          wp_register_script($this->_token . '-jquery-ui', esc_url($this->assets_url) . 'js/jquery-ui-1.10.3.custom.min.js', array('jquery'), $this->_version);
          wp_enqueue_script($this->_token . '-jquery-ui');
          wp_register_script($this->_token . '-jquery-touch-punch', esc_url($this->assets_url) . 'js/jquery.ui.touch-punch.min.js', array('jquery'), $this->_version);
          wp_enqueue_script($this->_token . '-jquery-touch-punch');
          wp_register_script($this->_token . '-bootstrap', esc_url($this->assets_url) . 'js/bootstrap.min.js', array('jquery'), $this->_version);
          wp_enqueue_script($this->_token . '-bootstrap');
          wp_register_script($this->_token . '-bootstrap-switch', esc_url($this->assets_url) . 'js/bootstrap-switch.js', array('jquery'), $this->_version);
          wp_enqueue_script($this->_token . '-bootstrap-switch');
          wp_register_script($this->_token . '-flatui-checkbox', esc_url($this->assets_url) . 'js/flatui-checkbox.js', array(), $this->_version);
          wp_enqueue_script($this->_token . '-flatui-checkbox');
          wp_register_script($this->_token . '-colpick', esc_url($this->assets_url) . 'js/colpick.js', 'jquery', $this->_version);
          wp_enqueue_script($this->_token . '-colpick');
          wp_register_script($this->_token . '-adminSettings', esc_url($this->assets_url) . 'js/admin_settings.js', 'jquery', $this->_version);
          wp_enqueue_script($this->_token . '-adminSettings');
          $textsJs = array('ON' => __('ON', 'WP_Visual_Chat'),
          'OFF' => __('OFF', 'WP_Visual_Chat'));
          wp_localize_script($this->_token . '-adminSettings', 'vcht_textsAdmin', $textsJs);
        }
        if (isset($_GET['page']) && ($_GET['page'] == 'vcht-settings' || $_GET['page'] == 'vcht-logsList')) {
            wp_enqueue_script('jquery');
            wp_enqueue_script('media-upload');
            wp_enqueue_script('thickbox');
        }
    }

    /*
     * display console page
     */

    public function submenu_console()
    {

        $settings = $this->getSettings();
        $current_user = wp_get_current_user();
        $userID = $current_user->ID;
        $user = $current_user->display_name;
        if (!$user || $user == "") {
            $user = $current_user->user_login;
        }
        $avatarOperator = get_avatar($userID, 37);

         $dispS = '';
        if(strlen($settings->purchaseCode)<3){
          $dispS = 'true';
        }
         echo '<div class="bootstraped"><div id="vcht_winActivation" class="modal fade " data-show="'.$dispS.'" >
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title">The license must be verified</h4>
                        </div>
                        <div class="modal-body">
                          <div id="vcht_iconLock"></div>
                          <p style="margin-bottom: 14px;">
                                  The license of this plugin isn\'t verified.<br/>Please fill the field below with your purchase code :
                          </p>
                          <div class="form-group">
                                  <input type="text" class="form-control" style="display:inline-block; width: 312px; margin-bottom: 4px" name="purchaseCode" placeholder="Enter your puchase code here"/>
                                  <a href="javascript:" onclick="vcht_checkLicense();" class="btn btn-primary"><span class="glyphicon glyphicon-check"></span>Verify</a>
                                  <br/>
                                  <span style="font-size:12px;"><a href="'.$this->parent->assets_url.'images/purchaseCode.gif" target="_blank">Where I can find my purchase code ?</a></span>
                          </div>
                          <div class="alert alert-danger" style="font-size:12px;  margin-bottom: 0px;" >
                                  <span class="glyphicon glyphicon-warning-sign" style="margin-right: 28px;float: left;font-size: 22px;margin-top: 10px;margin-bottom: 10px;"></span>
                            Each website using this plugin needs a legal license (1 license = 1 website). <br/>
                            You can find more information on envato licenses <a href="http://codecanyon.net/licenses/standard" target="_blank">clicking here</a>.<br/>
                               If you need to buy a new license of this plugin, <a href="http://codecanyon.net/item/wp-flat-visual-chat/8329900?ref=loopus" target="_blank">click here</a>.
                          </div>
                        </div>
                        <div class="modal-footer" style="text-align: center;">
                                                                  <a href="javascript:"  id="vcht_closeWinActivationBtn" class="btn btn-default disabled"><span class="glyphicon glyphicon-remove"></span><span class="vcht_text">Close</span></a>
                                                            </div><!-- /.modal-footer -->
                      </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                  </div><!-- /.modal -->
              </div><!-- /.bootstraped -->';
        ?>
        <iframe id="vcht_consoleFrame" src="<?php echo get_site_url(); ?>"></iframe>
        <div id="vcht_topPanel" class="bootstraped" <?php  if ($settings->enableInitiate == 0) {echo 'style="display:none;"';} ?>>
            <div class="container">
                <nav class="navbar navbar-inverse navbar-embossed" role="navigation">
                    <ul id="customersList" class="nav navbar-nav navbar-left ">
                        <li>
                            <label><span class="vcht_connectedUsersNb">0</span> <?php _e('users online', 'WP_Visual_Chat'); ?></label>
                            <div class="btn-group">
                                <button class="btn btn-primary dropdown-toggle" type="button"
                                        data-toggle="dropdown">
                                    <?php _e('Initiate a chat', 'WP_Visual_Chat'); ?> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                </ul>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div id="vcht_consolePanel" class="bootstraped">
            <div class="container">

                <nav class="navbar navbar-inverse navbar-embossed" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target="#navbar-collapse-01">
                            <span class="sr-only">Toggle navigation</span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-collapse-01">
                        <ul id="usersList" class="nav navbar-nav navbar-left ">
                            <li class="operatorTab text-center">
                                <?php echo $avatarOperator; ?>
                                <strong id="vcht_operatorname"></strong>
                                <input type="checkbox" id="vcht_onlineCB" data-toggle="switch"/>
                            </li>
                        </ul>
                        <ul class="nav pull-right">
                            <li>
                                <a href="javascript:" onclick="vcht_toggleChatPanel();" id="vcht_btnMinimize"><span
                                        class="glyphicon glyphicon-chevron-down"></span></a>
                            </li>
                            <li>
                                <a href="admin.php?page=vcht-logsList" onclick="vcht_operatorDisconnect();"><span
                                        class="glyphicon glyphicon-remove"></span></a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>

                <div id="vcht_consolePanelContent">
                        <span id="vcht_loader" class="ouro ouro2">
                            <span class="left"><span class="anim"></span></span>
                            <span class="right"><span class="anim"></span></span>
                        </span>

                    <div data-panel="chat" data-logid="0" class="row-fluid">
                        <div class="col-md-2 palette palette-wet-asphalt" id="vcht_userInfos">
                            <div class="vcht_avatarImg"></div>
                            <p>
                                <strong></strong>
                            </p>
                        </div>
                        <div class="col-md-9">
                            <div id="vcht_chatContent"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="vcht_infosPanel">

            <div class="container palette palette-wet-asphalt">
                <a href="javascript:" class="vcht_selectClose" onclick="vcht_stopSelectDomElement();"><span class="fui-cross"></span></a>
                <div class="col-md-12 text-center">

                </div>
            </div>
        </div>
        <audio id="vcht_audioMsg" controls data-enable="<?php
        if ($settings->playSound) {
            echo 'true';
        } else {
            echo 'false';
        }
        ?>">
            <source src="<?php echo $this->assets_url; ?>sound/message.ogg" type="audio/ogg">
            <source src="<?php echo $this->assets_url; ?>sound/message.mp3" type="audio/mpeg">
        </audio>
    <?php
    }
    
    /*
     * Return sentence
     */
    public function editSentence(){
        global $wpdb;
        $sentenceID = $_POST['sentenceID'];
        $table_name = $wpdb->prefix . "vcht_sentences";
        $sentences = $wpdb->get_results("SELECT * FROM $table_name WHERE id=$sentenceID LIMIT 1");
        if ($sentences[0]) {
            $sentence = $sentences[0];
            echo json_encode((array)$sentence);
        }
        die();
    }
    
    /*
     * Save sentence
     */
    public function saveSentence(){
        global $wpdb;
        $sentenceID = $_POST['sentenceID'];
        $table_name = $wpdb->prefix . "vcht_sentences";
        if($sentenceID>0){
            $wpdb->update($table_name, array('title' => sanitize_text_field(stripslashes($_POST['title'])),'shortcut' => sanitize_text_field($_POST['shortcut']), 'content' => sanitize_text_field(stripslashes($_POST['content']))), array('id' => $sentenceID));
        } else {
            $wpdb->insert($table_name, array('title' => sanitize_text_field(stripslashes($_POST['title'])),'shortcut' => sanitize_text_field($_POST['shortcut']), 'content' => sanitize_text_field(stripslashes($_POST['content']))));
        }
        die();
    }
    
    public function checkLicense(){
      global $wpdb;
      try {
          $url = 'http://www.loopus-plugins.com/updates/update.php?checkCode=8329900&code=' . $_POST['code'];
          $ch = curl_init($url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          $rep = curl_exec($ch);
          if ($rep != '0410') {
            $table_name = $wpdb->prefix . "vcht_settings";
            $wpdb->update($table_name, array('purchaseCode' => $_POST['code']), array('id' => 1));
          } else {
            echo '1';
          }
      } catch (Exception $e) {
          $table_name = $wpdb->prefix . "vcht_settings";
          $wpdb->update($table_name, array('purchaseCode' => $_POST['code']), array('id' => 1));
      }
      die();
    }

    /**
     * Return settings.
     * @access  public
     * @since   1.0.0
     * @return  void
     */
    public function getSettings()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "vcht_settings";
        $settings = $wpdb->get_results("SELECT * FROM $table_name WHERE id=1 LIMIT 1");
        if ($settings[0]) {
            return $settings[0];
        } else {
            return false;
        }
    }

    /**
     * Recover countries from ip
     * @access  public
     * @since   2.0
     * @return  void
     */
    function checkCountries()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "vcht_logs";
        $logs = $wpdb->get_results("SELECT * FROM $table_name WHERE ip!='' AND country=''");
        foreach ($logs as $log) {

            try {
                $country = 'unknow';
                $city = 'unknow';
                $xml = simplexml_load_file("http://www.geoplugin.net/xml.gp?ip=$log->ip");
                $country = $xml->geoplugin_countryName;
                $city = $xml->geoplugin_city;

                $wpdb->update($table_name, array('country' => $country, 'city' => $city), array('id' => $log->id));
            } catch (Exception $exc) {

            }
        }
    }

    /**
     * Menu import render
     * @return void
     */
    function submenu_import()
    {
        global $wpdb;
        ?>
        <div class="bootstraped">
        <div class="container-fluid">
        <div class="col-md-12 vcht_panelHeader">
          <span>WP Flat Visual Chat</span>
        </div>
        <div class="col-md-12">
        <h4><?php _e('Import data', 'WP_Visual_Chat'); ?></h4>

        <div class="wrap wpeImport">
            <?php
            $displayForm = true;
            $settings = $this->getSettings();
            //            $pageID = $settings->form_page_id;
            if (isset($_GET['import']) && isset($_FILES['importFile'])) {
                $error = false;
                if (!is_dir(plugin_dir_path(__FILE__) . '../tmp')) {
                    mkdir(plugin_dir_path(__FILE__) . '../tmp');
                    chmod(plugin_dir_path(__FILE__) . '../tmp', 0747);
                }
                $target_path = plugin_dir_path(__FILE__) . '../tmp/export_visual_chat.zip';
                if (@move_uploaded_file($_FILES['importFile']['tmp_name'], $target_path)) {


                    $upload_dir = wp_upload_dir();
                    if (!is_dir($upload_dir['path'])) {
                        mkdir($upload_dir['path']);
                    }

                    $zip = new ZipArchive;
                    $res = $zip->open($target_path);
                    if ($res === TRUE) {
                        $zip->extractTo(plugin_dir_path(__FILE__) . '../tmp/');
                        $zip->close();

                        $jsonfilename = 'export_visual_chat.json';
                        if (!file_exists(plugin_dir_path(__FILE__) . '../tmp/export_visual_chat.json')) {
                            $jsonfilename = 'export_visual_chat';
                        }

                        $file = file_get_contents(plugin_dir_path(__FILE__) . '../tmp/' . $jsonfilename);
                        $dataJson = json_decode($file, true);

                        $table_name = $wpdb->prefix . "vcht_settings";
                        $wpdb->query("TRUNCATE TABLE $table_name");
                        foreach ($dataJson['settings'] as $key => $value) {
                            $wpdb->insert($table_name, $value);
                        }

                        $table_name = $wpdb->prefix . "vcht_logs";
                        $wpdb->query("TRUNCATE TABLE $table_name");
                        foreach ($dataJson['logs'] as $key => $value) {
                            $wpdb->insert($table_name, $value);
                        }

                        $table_name = $wpdb->prefix . "vcht_messages";
                        $wpdb->query("TRUNCATE TABLE $table_name");
                        foreach ($dataJson['messages'] as $key => $value) {
                            $wpdb->insert($table_name, $value);
                        }

                        $table_name = $wpdb->prefix . "vcht_texts";
                        $wpdb->query("TRUNCATE TABLE $table_name");
                        foreach ($dataJson['texts'] as $key => $value) {
                            $wpdb->insert($table_name, $value);
                        }

                        $files = glob(plugin_dir_path(__FILE__) . '../tmp/*');
                        foreach ($files as $file) {
                            if (is_file($file))
                                unlink($file);
                        }

                        //$this->updateCSS();
                    } else {
                        $error = true;
                    }
                } else {
                    $error = true;
                }
                if ($error) {
                    echo '<div class="error">An error occurred during the transfer</div>';
                } else {
                    $displayForm = false;
                    echo '<div class="updated">Data has been imported.</div>';
                }
            }
            if ($displayForm) {
                ?>
                <p>
                    <?php _e('Import here the zip file created using the "Export" tool.', 'WP_Visual_Chat'); ?>

                </p>
                <div class="error" style="color: red;">
                    <?php _e('WARNING: import data will overwrite existing ones!', 'WP_Visual_Chat'); ?>
                </div>
                <form action="admin.php?page=vcht-import&import=1" method="post" enctype="multipart/form-data">
                    <p>
                        <input id="importFile" type="file" class="form-control" style="padding: 4px;padding-top: 8px;" name="importFile" placeholder="Select the .zip file"/>
                        <label for="importFile"> <span
                                class="description"><?php _e('Select the generated .zip file', 'WP_Visual_Chat'); ?></span>
                        </label>
                    </p>

                    <p>
                        <button type="submit" class="button-primary">
                            <?php _e('Import', 'WP_Visual_Chat'); ?>
                        </button>
                    </p>
                </form>
            <?php
            }
            ?>
        </div>
    </div>
</div>
</div>
    <?php
    }

    /**
     * Menu export render
     * @return void
     */
    function submenu_export()
    {
        global $wpdb;

        if (!is_dir(plugin_dir_path(__FILE__) . '../tmp')) {
            mkdir(plugin_dir_path(__FILE__) . '../tmp');
            chmod(plugin_dir_path(__FILE__) . '../tmp', 0747);
        }

        $destination = plugin_dir_path(__FILE__) . '../tmp/export_visual_chat.zip';
        $zip = new ZipArchive();
        if (file_exists($destination)) {
            unlink($destination);
        }
        if ($zip->open($destination, ZipArchive::CREATE) !== true) {
            return false;
        }

        $jsonExport = array();
        $table_name = $wpdb->prefix . "vcht_settings";
        $settings = $wpdb->get_results("SELECT * FROM $table_name WHERE id=1 LIMIT 1");
        $jsonExport['settings'] = $settings;

        $table_name = $wpdb->prefix . "vcht_logs";
        $steps = array();
        foreach ($wpdb->get_results("SELECT * FROM $table_name") as $key => $row) {
            $steps[] = $row;
        }
        $jsonExport['logs'] = $steps;

        $table_name = $wpdb->prefix . "vcht_messages";
        $items = array();
        foreach ($wpdb->get_results("SELECT * FROM $table_name") as $key => $row) {
            $items[] = $row;
        }
        $jsonExport['messages'] = $items;

        $table_name = $wpdb->prefix . "vcht_texts";
        $items = array();
        foreach ($wpdb->get_results("SELECT * FROM $table_name") as $key => $row) {
            $items[] = $row;
        }
        $jsonExport['texts'] = $items;

        $fp = fopen(plugin_dir_path(__FILE__) . '../tmp/export_visual_chat.json', 'w');
        fwrite($fp, json_encode($jsonExport));
        fclose($fp);

        $zip->addfile(plugin_dir_path(__FILE__) . '../tmp/export_visual_chat.json', 'export_visual_chat.json');
        $zip->close();
        ?>

        <div class="bootstraped">
        <div class="container-fluid">
        <div class="col-md-12 vcht_panelHeader">
          <span>WP Flat Visual Chat</span>
        </div>
        <div class="col-md-12">
        <h4><?php _e('Export data', 'WP_Visual_Chat'); ?></h4>
        <div class="wrap wpeExport">
            <p>
                <?php _e('Export all this plugin data to a zip file will can be imported on another website.', 'WP_Visual_Chat'); ?>

            </p>

            <p>
                <a download class="btn btn-primary" href="<?php echo esc_url(trailingslashit(plugins_url('/', $this->parent->file))) . 'tmp/export_visual_chat.zip'; ?>"><?php _e('Export', 'WP_Visual_Chat'); ?></a>
            </p>
        </div>
    <?php
    }

    /*
     * Display texts pages
     */

    public function submenu_texts()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "vcht_texts";
        $texts = $wpdb->get_results("SELECT * FROM $table_name ORDER BY id ASC");
        ?>
        <div class="wrap">
            <h3>Chat texts</h3>

            <div id="vcht_response"></div>
            <form id="form_texts" method="post" action="#" onsubmit="qc_process(this);return false;">
                <table class="form-table">
                    <?php
                    foreach ($texts as $text) {
                        echo '<tr>
                                <td colspan="2">';
                        if ($text->isTextarea) {
                            echo '<textarea style="width: 340px;" id="t_' . $text->id . '" name="t' . $text->id . '"  placeholder="' . $text->original . '" >' . $text->content . '</textarea>';
                        } else {
                            echo '<input style="width: 340px;" id="t_' . $text->id . '" type="text" name="t' . $text->id . '"  placeholder="' . $text->original . '" value="' . $text->content . '">';
                        }
                        echo '<label for="t_' . $text->id . '">
                                        <span class="description">Original text : <i>' . $text->original . '</i></span>
                                    </label>
                                </td>
                            </tr>';
                    }
                    ?>
                    <tr>
                        <td colspan="2">
                            <input type="submit" value="Save" class="button-primary"/>
                        </td>
                    </tr>
                </table>
                <script>
                    function qc_process(e) {
                        var error = false;
                        if (!error) {
                            jQuery("#vcht_response").hide();
                            var data = {action: "vcht_texts_save"};
                            jQuery('#form_texts input, #form_texts textarea').each(function () {
                                if (jQuery(this).attr('name')) {
                                    eval('data.' + jQuery(this).attr('name') + ' = jQuery(this).val();');
                                }
                            });

                            jQuery.post(ajaxurl, data, function (response) {
                                jQuery("#vcht_response").html('<div id="message" class="updated"><p>Texts <strong>saved</strong>.</p></div>');
                                jQuery("#vcht_response").fadeIn(250);
                            });
                        }
                    }
                </script>
            </form>
        </div>
    <?php
    }

    /*
     * Save texts
     */

    public function texts_save()
    {
        global $wpdb;
        $response = "Error, try again later.";
        $table_name = $wpdb->prefix . "vcht_texts";
        $sqlDatas = array();
        foreach ($_POST as $key => $value) {
            if ($key != 'action') {
              if($key == "set_usePoFile") {
                $wpdb->update($wpdb->prefix . "vcht_settings", array('usePoFile' =>$value), array('id' => 1));
              } else {
                $key = substr($key, 1);
                $wpdb->update($table_name, array('content' => stripslashes($value)), array('id' => $key));
              }
            }
        }
        $response = '<div id="message" class="updated"><p>' . __('Texts saved.', 'WP_Visual_Chat') . '</p></div>';

        echo $response;
        die();
    }

    /*
     * display settings page
     */

    public function submenu_settings()
    {
        global $wpdb;
        $settings = $this->getSettings();
        ?>
        <div class="bootstraped">
        <div class="container-fluid">
        <div class="col-md-12 vcht_panelHeader">
          <span>WP Flat Visual Chat</span>
        </div>
        <div class="col-md-12">
        <h4><?php _e('Chat Settings', 'WP_Visual_Chat'); ?></h4>

        <div id="vcht_response"></div>
        <h2 class="nav-tab-wrapper" style="margin-bottom: 0px;">
            <?php
            if (isset($_GET['tab']))
                $tab = $_GET['tab'];
            else
                $tab = 'general';

            $active = '';
            if ($tab == 'general') {
                $active = 'nav-tab-active';
            }
            echo '<a class="nav-tab ' . $active . '" href="?page=vcht-settings&tab=general">' . __('General', 'WP_Visual_Chat') . '</a>';
            $active = '';
            if ($tab == 'colors') {
                $active = 'nav-tab-active';
            }
            echo '<a class="nav-tab ' . $active . '" href="?page=vcht-settings&tab=colors">' . __('Colors', 'WP_Visual_Chat') . '</a>';
            $active = '';
            if ($tab == 'texts') {
                $active = 'nav-tab-active';
            }
            echo '<a class="nav-tab ' . $active . '" href="?page=vcht-settings&tab=texts">'. __('Texts', 'WP_Visual_Chat') . '</a>';
            
            $active = '';
            if ($tab == 'sentences') {
                $active = 'nav-tab-active';
            }
            echo '<a class="nav-tab ' . $active . '" href="?page=vcht-settings&tab=sentences">'. __('Preset sentences', 'WP_Visual_Chat') . '</a>';
            ?>
        </h2>

        <form id="form_settings" method="post" action="#" onsubmit="qc_process(this);
                    return false;">
        <input id="id" type="hidden" name="id" value="1">
        <?php
        switch ($tab) {
            case 'general' :
                ?>
                <table class="form-table">
                <tr style="display:none">
                    <th scope="row">PageID</th>
                    <td>
                        <input id="pageID" type="text" name="pageID" value="<?php echo $settings->pageID; ?>">
                        <label for="pageID">
                            <span class="description"><?php _e('Select an empty page', 'WP_Visual_Chat'); ?></span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Purchase License Code', 'WP_Visual_Chat'); ?></th>
                    <td>
                        <input id="purchaseCode" type="text" name="purchaseCode"
                               placeholder="<?php _e('Enter the purchase license', 'WP_Visual_Chat'); ?>"
                               value="<?php echo $settings->purchaseCode; ?>">
                        <label for="purchaseCode">
                            <span class="description"><a
                                    href="<?php echo $this->parent->assets_url; ?>images/purchaseCode.gif"
                                    target="_blank"><?php _e('How to find my purchase code ?', 'WP_Visual_Chat'); ?> </a></span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Default operator picture', 'WP_Visual_Chat'); ?> </th>
                    <td>
                        <input id="chatLogo" type="text" name="chatLogo" value="<?php
                        echo $settings->chatLogo;
                        ?>"/>
                        <input class="imageBtn button" type="button" value="Upload Image"/>
                        <label for="chatLogo">
                            <span class="description"><?php _e('Choose a picture', 'WP_Visual_Chat'); ?></span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Enable feature "Initiate a chat" ?', 'WP_Visual_Chat'); ?> </th>
                    <td>
                        <select id="enableInitiate" name="enableInitiate">
                           <option value="0" <?php
                           if ($settings->enableInitiate == 0) {
                               echo 'selected';
                           }
                           ?>><?php _e('No','WP_Visual_Chat'); ?></option>
                            <option value="1"<?php
                            if ($settings->enableInitiate == 1) {
                                echo 'selected';
                            }
                            ?>><?php _e('Yes','WP_Visual_Chat'); ?></option>
                        </select>
                        <label for="enableInitiate">
                            <span class="description"><?php _e('Try to disable it if you are experiencing slowdowns', 'WP_Visual_Chat'); ?></span>
                        </label>
                    </td>
                </tr>

                <tr>
                    <th scope="row"><?php _e('Default customer picture', 'WP_Visual_Chat'); ?></th>
                    <td>
                        <input id="chatDefaultPic" type="text" name="chatDefaultPic" value="<?php
                        echo $settings->chatDefaultPic;
                        ?>"/>
                        <input class="imageBtn button" type="button" value="Upload Image"/>
                        <label for="chatDefaultPic">
                            <span class="description"><?php _e('Choose a picture', 'WP_Visual_Chat'); ?></span>
                        </label>
                    </td>
                </tr>

                <tr>
                    <th scope="row"><?php _e('Play sound as notification ?', 'WP_Visual_Chat'); ?></th>
                    <td>
                        <select id="playSound" name="playSound">
                            <option value="0"><?php _e('No', 'WP_Visual_Chat'); ?></option>
                            <option value="1" <?php
                            if ($settings->playSound) {
                                echo 'selected';
                            }
                            ?>><?php _e('Yes', 'WP_Visual_Chat'); ?></option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Chat position', 'WP_Visual_Chat'); ?></th>
                    <td>
                        <select id="chatPosition" name="chatPosition">
                            <option value="right"><?php _e('Right', 'WP_Visual_Chat'); ?></option>
                            <option value="left" <?php
                            if ($settings->chatPosition == 'left') {
                                echo 'selected';
                            }
                            ?>><?php _e('Left', 'WP_Visual_Chat'); ?></option>
                        </select>
                    </td>
                </tr>


                <tr>
                    <th scope="row"><?php _e('Admin email', 'WP_Visual_Chat'); ?></th>
                    <td>
                        <input id="adminEmail" type="email" name="adminEmail" value="<?php
                        echo $settings->adminEmail;
                        ?>"/>
                        <label for="adminEmail">
                            <span
                                class="description"><?php _e('Enter the main contact email', 'WP_Visual_Chat'); ?></span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Contact email subject', 'WP_Visual_Chat'); ?></th>
                    <td>
                        <input id="emailSubject" type="text" name="emailSubject" value="<?php
                        echo $settings->emailSubject;
                        ?>"/>
                        <label for="emailSubject">
                            <span
                                class="description"><?php _e('This is the subject of the email when a user contacts you', 'WP_Visual_Chat'); ?></span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Roles allowed to chat', 'WP_Visual_Chat'); ?></th>
                    <td>
                        <?php
                        global $wp_roles;
                        foreach ($wp_roles->roles as $role) {
                            $selected = '';
                            $disabled = '';
                            if (array_key_exists('visual_chat', $role['capabilities']) && $role['capabilities']['visual_chat'] == 1) {
                                $selected = 'checked';
                            }
                            if (strtolower($role['name']) == 'administrator') {
                                $disabled = 'disabled';
                            }
                            if (strtolower($role['name']) == 'chat operator') {
                                $selected = 'checked';
                                $disabled = 'disabled';
                            }
                            echo '<p><input name="rolesAllowed" value="' . ($role['name']) . '"  data-toggle="switch" type="checkbox" ' . $selected . ' ' . $disabled . ' /><span style="padding-left: 16px;">' . $role['name'] . '</span></p>';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Bounce FX', 'WP_Visual_Chat'); ?></th>
                    <td>
                        <select id="bounceFx" name="bounceFx">
                            <option value="0" <?php
                            if (!$settings->bounceFx) {
                                echo 'selected';
                            }
                            ?>><?php _e('No', 'WP_Visual_Chat'); ?></option>
                            <option value="1" <?php
                            if ($settings->bounceFx) {
                                echo 'selected';
                            }
                            ?>><?php _e('Yes', 'WP_Visual_Chat'); ?></option>
                        </select>
                        <label for="emailSubject">
                            <span
                                class="description"><?php _e('Give a bounce effect to the chat ?', 'WP_Visual_Chat'); ?></span>
                        </label>
                    </td>
                </tr>
            <tr>
                <td colspan="2">
                    <a href="javascript:" class="btn btn-primary" onclick="jQuery('#form_settings').submit();"><span class="glyphicon glyphicon-floppy-disk"></span><?php echo __('Save','WP_Visual_Chat'); ?></a>
                </td>
            </tr>
            </table>

                <?php
                break;
            case 'colors' :
                ?>
                <table class="form-table">
                <tr>
                    <th scope="row"><?php _e('Default text color', 'WP_Visual_Chat'); ?></th>
                    <td>
                        <input id="colorB" type="text" name="colorB" class="colorpick" placeholder="Enter the color hex"
                               value="<?php echo $settings->colorB; ?>">
                        <label for="colorB">
                            <span class="description"><?php _e('ex', 'WP_Visual_Chat'); ?>  : #34495E</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('User bubble background color and header', 'WP_Visual_Chat'); ?></th>
                    <td>
                        <input id="colorA" type="text" name="colorA" class="colorpick" placeholder="Enter the color hex"
                               value="<?php echo $settings->colorA; ?>">
                        <label for="colorA">
                            <span class="description"><?php _e('ex', 'WP_Visual_Chat'); ?> : #1abc9c</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('User bubble text color', 'WP_Visual_Chat'); ?></th>
                    <td>
                        <input id="colorE" type="text" name="colorE" class="colorpick" placeholder="Enter the color hex"
                               value="<?php echo $settings->colorE; ?>">
                        <label for="colorE">
                            <span class="description"><?php _e('ex', 'WP_Visual_Chat'); ?>  : #FFFFFF</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Operator bubble background color', 'WP_Visual_Chat'); ?></th>
                    <td>
                        <input id="colorC" type="text" name="colorC" class="colorpick" placeholder="Enter the color hex"
                               value="<?php echo $settings->colorC; ?>">
                        <label for="colorC">
                            <span class="description"><?php _e('ex', 'WP_Visual_Chat'); ?>  : #ECF0F1</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Operator bubble text color', 'WP_Visual_Chat'); ?></th>
                    <td>
                        <input id="colorF" type="text" name="colorF" class="colorpick" placeholder="Enter the color hex"
                               value="<?php echo $settings->colorF; ?>">
                        <label for="colorF">
                            <span class="description"><?php _e('ex', 'WP_Visual_Chat'); ?>  : #bdc3c7</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Default button color', 'WP_Visual_Chat'); ?></th>
                    <td>
                        <input id="colorD" type="text" name="colorD" class="colorpick" placeholder="Enter the color hex"
                               value="<?php echo $settings->colorD; ?>">
                        <label for="colorD">
                            <span class="description"><?php _e('ex', 'WP_Visual_Chat'); ?>  : #cacfd2</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Main button color', 'WP_Visual_Chat'); ?></th>
                    <td>
                        <input id="colorG" type="text" name="colorG" class="colorpick" placeholder="Enter the color hex"
                               value="<?php echo $settings->colorG; ?>">
                        <label for="colorG">
                            <span class="description"><?php _e('ex', 'WP_Visual_Chat'); ?>  : #1abc9c</span>
                        </label>
                    </td>
                </tr>

                <tr>
                    <th scope="row"><?php _e('Shining element color', 'WP_Visual_Chat'); ?></th>
                    <td>
                        <input id="shineColor" type="text" name="shineColor" class="colorpick"
                               placeholder="Enter the color hex" value="<?php echo $settings->shineColor; ?>">
                        <label for="shineColor">
                            <span class="description"><?php _e('ex', 'WP_Visual_Chat'); ?>  : #1abc9c</span>
                        </label>
                    </td>
                </tr>
            </table>            
                <p >
                    <a href="javascript:" class="btn btn-primary" onclick="jQuery('#form_settings').submit();"><span class="glyphicon glyphicon-floppy-disk"></span><?php echo __('Save','WP_Visual_Chat'); ?></a>
                </p>
                <?php
                break;
            case 'texts':
                echo '<table class="form-table">
                      <tr>';
                $sel = '';
                if ($settings->usePoFile == 1){
                  $sel = 'checked';
                }
                echo '<td colspan="2">
                  <label style="margin-right: 8px; margin-bottom: 16px; margin-bottom: 16px;">'.__('Use .po file / WPML ?', 'WP_Visual_Chat').'</label>
                    <input type="checkbox" '.$sel.' data-toggle="switch" id="set_usePoFile" name="set_usePoFile"/>
                        ';
                echo '</td>';
                echo '</tr>';

                $table_name = $wpdb->prefix . "vcht_texts";
                $texts = $wpdb->get_results("SELECT * FROM $table_name ORDER BY id ASC");

                foreach ($texts as $text) {
                    echo '<tr class="vcht_rowTxt">
                                <td colspan="2">';
                    if ($text->isTextarea) {
                        echo '<textarea style="width: 340px;" id="t_' . $text->id . '" name="t' . $text->id . '"  placeholder="' . $text->original . '" >' . $text->content . '</textarea>';
                    } else {
                        echo '<input style="width: 340px;" id="t_' . $text->id . '" type="text" name="t' . $text->id . '"  placeholder="' . $text->original . '" value="' . $text->content . '"/>';
                    }
                    echo '<label for="t_' . $text->id . '">
                                        <span class="description">'.__('Original text','WP_Visual_Chat').' : <i>' . $text->original . '</i></span>
                                    </label>
                                </td>
                            </tr>';
                }
                echo '<tr class="vcht_rowNotxt">
                  <td colspan="2"><p>'.__('You can find the .po file in the language/ folder .','WP_Visual_Chat').'<br/>
                    <a href="http://poedit.net/" target="_blank">'.__('Click here to download poedit','WP_Visual_Chat').'</a>
                  </p></td>
                </tr>
                </table>
                <p>
                    <a href="javascript:" class="btn btn-primary" onclick="jQuery(\'#form_settings\').submit();"><span class="glyphicon glyphicon-floppy-disk"></span>'.__('Save','WP_Visual_Chat').'</a>
                </p>
                ';

                break; 
                case 'sentences' :
                    
                    if(isset($_GET['delSentence'])){
                        $table_name = $wpdb->prefix . "vcht_sentences";
                        $wpdb->delete($table_name, array('id' => $_GET['delSentence']));                        
                    }
                ?>                
                <table class="wp-list-table widefat fixed striped">
                    <thead>
                        <tr>
                            <th><?php echo __('Title','WP_Visual_Chat'); ?></th>
                            <th><?php echo __('Keyboard shortcut','WP_Visual_Chat'); ?></th>
                            <th><?php echo __('Sentence','WP_Visual_Chat'); ?></th>
                            <th><?php echo __('Actions','WP_Visual_Chat'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $table_name = $wpdb->prefix . "vcht_sentences";
                    $sentences = $wpdb->get_results("SELECT * FROM $table_name ORDER BY id ASC");
                    foreach ($sentences as $sentence) {
                        echo '<tr>';
                        // TODO : finir tableau
                        echo '<td>'.$sentence->title.'</td>';
                        echo '<td>SHIFT + '.$sentence->shortcut.'</td>';
                        echo '<td>'.$sentence->content.'</td>';
                        echo '<td><a href="javascript:" onclick="vcht_editSentence('.$sentence->id.');" class="btn btn-primary btn-circle"><span class="glyphicon glyphicon-pencil"></span></a>';
                        echo '<a href="admin.php?page=vcht-settings&tab=sentences&delSentence='.$sentence->id.'" class="btn btn-danger btn-circle"><span class="glyphicon glyphicon-trash"></span></a></td>';
                        echo '</tr>';                        
                    }
                    ?>
                    </tbody>
                 
                </table>
                <p style="margin-top: 22px;">
                    <a href="javascript:" class="btn btn-primary" onclick="jQuery('#form_settings').submit();"><span class="glyphicon glyphicon-floppy-disk"></span><?php echo __('Save','WP_Visual_Chat'); ?></a>
                    <a href="javascript:" class="btn btn-default" onclick="vcht_editSentence(0);" style="margin-left: 8px;"><span class="glyphicon glyphicon-plus"></span><?php echo __('Add a new sentence','WP_Visual_Chat'); ?></a>
                </p>
                
                <?php
                break;
        }
        ?>
       
        </form>
        <div id="vcht_winEditSentence" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title"><?php echo __('Edit a sentence','WP_Visual_Chat'); ?></h4>
                </div>
                <div class="modal-body" style="padding-bottom: 0px;">
                    <div class="form-group">
                        <label><?php echo __('Title','WP_Visual_Chat'); ?> :</label>
                        <input type="text" name="sentence_title" maxlength="20" class="form-control1" />
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Keyboard shortcut','WP_Visual_Chat'); ?> :</label>
                        SHIFT + 
                        <input type="text" name="sentence_shortcut" maxlength="1" class="form-control1" style="margin-left: 8px;" />
                    </div>
                    <div class="form-group" style="margin-bottom: 0px;">
                        <label><?php echo __('Sentence','WP_Visual_Chat'); ?> :</label>
                        <textarea name="sentence_content" class="form-control1" ></textarea>
                    </div>
                    <div style="padding: 14px; background-color: #bdc3c7; color: #FFF; font-size: 16px; text-align: center;">
                        <p>
                            <?php echo __('Shortcodes you can use in sentence','WP_Visual_Chat');?> :
                        </p>
                        <ul>
                            <li><strong>[user]</strong> : <?php echo __('Displays the username','WP_Visual_Chat');?></li>
                            <li><strong>[operator]</strong> : <?php echo __('Displays the operator name','WP_Visual_Chat');?></li>
                            <li><strong>[siteurl]</strong> : <?php echo __('Displays the website url','WP_Visual_Chat');?></li>

                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:" onclick="vcht_saveSentence();" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span><?php echo __('Save','WP_Visual_Chat'); ?></a>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->
          
          
        </div>
        </div>
        </div>
        <script>
            var formfield;
            jQuery(document).ready(function () {
                window.old_tb_remove = window.tb_remove;
                window.tb_remove = function () {
                    window.old_tb_remove();
                    formfield = null;
                };
                window.original_send_to_editor = window.send_to_editor;
                window.send_to_editor = function (html) {
                    if (formfield) {
                        fileurl = jQuery('img', html).attr('src');
                        jQuery(formfield).val(fileurl);
                        tb_remove();
                    } else {
                        window.original_send_to_editor(html);
                    }
                };
                jQuery('.imageBtn').click(function () {
                    formfield = jQuery(this).prev('input');
                    tb_show('', 'media-upload.php?TB_iframe=true');

                    return false;

                });
                jQuery('#set_usePoFile').change(function(){
                  if (!jQuery('#set_usePoFile').is(':checked')){
                      jQuery('.vcht_rowTxt').slideDown();
                      jQuery('.vcht_rowNotxt').slideUp();
                  } else {
                      jQuery('.vcht_rowTxt').slideUp();
                      jQuery('.vcht_rowNotxt').slideDown();
                  }
                });
            });
            <?php
            if ($tab == 'texts') {
                ?>
            function qc_process(e) {
                var error = false;
                if (!error) {
                    jQuery("#vcht_response").hide();
                    var data = {
                        action: "vcht_texts_save"
                    };
                    jQuery('#form_settings input, #form_settings textarea, #form_settings select').each(function () {
                        if (jQuery(this).attr('name')) {
                          var val = jQuery(this).val();
                          if(jQuery(this).attr('name') == 'set_usePoFile'){
                            val = 0;
                            if(jQuery(this).is(':checked')){
                              val = 1;
                            }
                          }
                            eval('data.' + jQuery(this).attr('name') + ' = val;');
                        }
                    });
                    jQuery.post(ajaxurl, data, function (response) {
                        jQuery("#vcht_response").html('<div id="message" class="updated"><p>Texts <strong>saved</strong>.</p></div>');
                        jQuery("#vcht_response").fadeIn(250);
                    });
                }
            }
            <?php
        } else {
            ?>
            function qc_process(e) {

                var error = false;
                if (!error) {
                    jQuery("#vcht_response").hide();
                    var data = {action: "vcht_settings_save"};
                    jQuery('#form_settings input, #form_settings select, #form_settings textarea').each(function () {
                        if (jQuery(this).attr('name') && jQuery(this).attr('name') != 'rolesAllowed') {
                            eval('data.' + jQuery(this).attr('name') + ' = jQuery(this).val();');
                        }
                    });
                    var rolesAllowed = '';
                    jQuery('input[name=rolesAllowed]:checked').each(function () {
                        rolesAllowed += jQuery(this).val() + ',';
                    });
                    if (rolesAllowed.length > 0) {
                        rolesAllowed = rolesAllowed.substr(0, rolesAllowed.length - 1);
                    }
                    data.rolesAllowed = rolesAllowed;
                    jQuery.post(ajaxurl, data, function (response) {
                        jQuery("#vcht_response").html('<div id="message" class="updated"><p>Settings <strong>saved</strong>.</p></div>');
                        jQuery("#vcht_response").fadeIn(250);
                        setTimeout(function () {
                            document.location.href = document.location.href;
                        }, 400);
                    });
                }
            }
            <?php } ?>
        </script>
    <?php
    }

    private function isUpdated()
    {
        $settings = $this->getSettings();
        if ($settings->updated) {
            return false;
        } else {
            return true;
        }
    }


    /**
     * save settings
     * @return void
     */
    public function settings_save()
    {
        global $wpdb;
        global $wp_roles;
        $response = "Error, try again later.";
        $table_name = $wpdb->prefix . "vcht_settings";
        $sqlDatas = array();
        foreach ($_POST as $key => $value) {
            if ($key != 'action') {
                $sqlDatas[$key] = stripslashes($value);
            }
        }
        $wpdb->update($table_name, $sqlDatas, array('id' => 1));
        $response = '<div id="message" class="updated"><p>' . __('Settings saved.', 'WP_Visual_Chat') . '</p></div>';
        //   $this->updateCSS();
        echo $response;
        die();
    }

    /**
     * update colors CSS
     * @return void
     */
    private function updateCSS()
    {
        $settings = $this->getSettings();

        chmod(plugin_dir_path(__FILE__) . '../assets/css/colors.css', 0747);
        chmod(plugin_dir_path(__FILE__) . '../assets/css/colors_front.css', 0747);
        chmod(plugin_dir_path(__FILE__) . '../assets/css/colors_admin.css', 0747);
        $colorsStyles = '
        body {
            color: ' . $settings->colorB . ';
        }
        .vcht_chat .palette-turquoise,.btn-primary,.btn-primary:hover,.btn-primary:active {
            background-color: ' . $settings->colorA . ';
            color: ' . $settings->colorE . ';
        }
        .vcht_chat .bubble_right .bubble_arrow {
            border-color: transparent transparent transparent ' . $settings->colorA . ';
        }
        .vcht_chat .palette-clouds {
            background-color: ' . $settings->colorC . ';
            color: ' . $settings->colorF . ';
        }
        .vcht_chat .form-control:focus {
            border-color: ' . $settings->colorA . ';
        }
        .vcht_chat .anim {
            background: none repeat scroll 0 0 ' . $settings->colorA . ';
        }
        .vcht_chat .btn-default,.btn-default:hover,.btn-default:active {
            background-color: ' . $settings->colorD . ';
        }
        .vcht_chat .form-control {
            border-color: ' . $settings->colorD . ';
        }
        .vcht_chat .bubble_left .bubble_arrow {
            border-color: transparent ' . $settings->colorC . ' transparent transparent;
        }
        .vcht_selectedDom {
            -moz-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' ;
            -webkit-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' ;
            -o-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' ;
            box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' ;
        }
        .nicescroll-rails > div {
            background-color: ' . $settings->colorD . ' !important;
        }
        .avatarImg {
            background-image: url(' . $settings->chatDefaultPic . ');
        }

        @-o-keyframes glow {
            0% {
                -o-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';
            }
            50% {
                -o-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ';
            }
            100% {
                -o-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';
            }
        }
        @-moz-keyframes glow {
            0% {
                -moz-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';
            }
            50% {
                -moz-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ';
            }
            100% {
                -moz-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';
            }
        }
        @-webkit-keyframes glow {
            0% {
                -webkit-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';
            }
            50% {
                -webkit-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ';
            }
            100% {
                -webkit-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';
            }
        }
        @keyframes glow {
            0% {
                box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ' ;
            }
            50% {
                box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' ;
            }
            100% {
                box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ' ;
            }
        }
        ';

        $fp = fopen(plugin_dir_path(__FILE__) . '../assets/css/colors.css', 'w');
        fwrite($fp, $colorsStyles);
        fclose($fp);

        $colorsStyles2 = '
        .vcht_selectedDom {
            -moz-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' !important;
            -webkit-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' !important;
            -o-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' !important ;
            box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' !important ;
        }
        @-o-keyframes glow {
            0% {
                -o-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';
            }
            50% {
                -o-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ';
            }
            100% {
                -o-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';
            }
        }
        @-moz-keyframes glow {
            0% {
                -moz-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';
            }
            50% {
                -moz-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ';
            }
            100% {
                -moz-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';
            }
        }
        @-webkit-keyframes glow {
            0% {
                -webkit-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';
            }
            50% {
                -webkit-box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ';
            }
            100% {
                -webkit-box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ';
            }
        }
        @keyframes glow {
            0% {
                box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ' ;
            }
            50% {
                box-shadow: 0px 0px 40px 0px ' . $settings->shineColor . ' ;
            }
            100% {
                box-shadow: 0px 0px 10px 0px ' . $settings->shineColor . ' ;
            }
        }';
        $fp2 = fopen(plugin_dir_path(__FILE__) . '../assets/css/colors_front.css', 'w');
        fwrite($fp2, $colorsStyles2);
        fclose($fp2);

        $colorsStyles3 = '
        .bubble_photo {
            background-image: url(' . $settings->chatDefaultPic . ');
        }';
        $fp3 = fopen(plugin_dir_path(__FILE__) . '../assets/css/colors_admin.css', 'w');
        fwrite($fp3, $colorsStyles3);
        fclose($fp3);
    }

    /*
     * display logs list page
     */

    public function submenu_logsList()
    {
        global $wpdb;
      //  $this->checkCountries();
        echo '<div class="bootstraped">
        <div class="container-fluid">
        <div class="col-md-12 vcht_panelHeader">
          <span>WP Flat Visual Chat</span>
        </div>
        <div class="col-md-12">';

        if (isset($_GET['log'])) {
            $logID = $_GET['log'];
            $table_name = $wpdb->prefix . "vcht_logs";
            $rows = $wpdb->get_results("SELECT * FROM $table_name WHERE id=$logID LIMIT 1");
            $logDatas = $rows[0];
            /* $table_name = $wpdb->prefix . "vcht_messages";
              $messages = $wpdb->get_results("SELECT * FROM $table_name WHERE logID=$logID ORDER BY id ASC"); */
            ?>
            <div class="wrap">
            <div id="icon-users" class="icon32"></div>
            <h4><?php _e('Chat Log Details', 'WP_Visual_Chat'); ?></h4>
            <?php
            echo '<p>User : <strong>' . $logDatas->username . '</strong></p>';
            echo '<p>Date : <strong>' . $logDatas->date . '</strong></p>';

            $msgList = new vcht_MsgTable();
            $msgList->logID = $logID;
            $msgList->prepare_items();
            $msgList->display();
        } else {


            if (isset($_GET['remove'])) {
                $table_name = $wpdb->prefix . "vcht_logs";
                $wpdb->delete($table_name, array('id' => $_GET['remove']));
                $table_name = $wpdb->prefix . "vcht_messages";
                $wpdb->delete($table_name, array('logID' => $_GET['remove']));
            }

            $logsList = new vcht_LogsTable();
            if (isset($_GET['userID'])) {
                $logsList->userID = $_GET['userID'];
            }
            if (isset($_GET['search'])) {
                $logsList->search = $_GET['search'];
            }

            $logsList->prepare_items();
            ?>
            <div class="wrap">
                <div id="icon-users" class="icon32"></div>
                <h4><?php _e('Chat Log', 'WP_Visual_Chat'); ?></h4>

                <p>
                    <label><?php _e('Filter by User :', 'WP_Visual_Chat'); ?> </label>
                    <?php
                    if (isset($_GET['userID']) && $_GET['userID'] != '0') {
                        wp_dropdown_users(array('show_option_all' => __('All', 'WP_Visual_Chat'), 'selected' => $_GET['userID']));
                    } else {
                        wp_dropdown_users(array('show_option_all' => __('All', 'WP_Visual_Chat')));
                    }
                    ?>
                    <label><?php _e('or do a custom search :', 'WP_Visual_Chat'); ?> </label>
                    <input id="filter_custom" type="text" style="width: 260px;"
                           placeholder="<?php _e('Enter a username or country, ip etc ...', 'WP_Visual_Chat'); ?>"
                           value="<?php if (isset($_GET['search'])) {
                               echo $_GET['search'];
                           } ?>"/> <a href="javascript:" onclick="vcht_customFilter();"
                                      class="button-primary"><?php _e('Search', 'WP_Visual_Chat'); ?></a>

                </p>
                <?php $logsList->display(); ?>
            </div>
        <?php
        }
        ?>
    </div>
</div>
</div>
        <script>
            function vcht_testIframe() {
                try {
                    return window.self !== window.top;
                } catch (e) {
                    return true;
                }
            }
            jQuery(document).ready(function () {
                jQuery('#user').change(function () {
                    if (jQuery('#user').val() != '0') {
                        document.location.href = 'admin.php?page=vcht-logsList&userID=' + jQuery('#user').val();
                    } else {
                        document.location.href = 'admin.php?page=vcht-logsList';
                    }
                });
                if (vcht_testIframe()) {
                    jQuery('#adminmenuwrap,#adminmenuback').hide();
                    jQuery('#wpcontent, #wpfooter').css('margin-left', '0px');
                }
                jQuery('#filter_custom').keyup(function () {
                    jQuery('#user').val('0');
                });
            });
        </script>
    <?php
    }

    /*
     * Activate license
     */
    private function activateLicense()
    {
        global $wpdb;
        $settings = $this->getSettings();
        $url = 'http://www.loopus-plugins.com/updates/update.php?confirmUpdate=8329900&version=' . $settings->purchaseCode . '&ip=' . $_SERVER['SERVER_ADDR'] . '&url=' . get_site_url();
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $rep = curl_exec($ch);
        if (!$rep || $rep == '1') {
            $wpdb->update($wpdb->prefix . "vcht_settings", array('updated' => 0), array('id' => 1));
            setcookie('pll_updateC', 1, time() + 60 * 60 * 24 * 1);
        } else if ($rep == 'needupdate') {
            $wpdb->update($wpdb->prefix . "vcht_settings", array('updated' => 1), array('id' => 1));
            setcookie('pll_updateC', 2, time() + 60 * 60 * 24 * 1);
        } else {
            $wpdb->update($wpdb->prefix . "vcht_settings", array('updated' => 1), array('id' => 1));
            setcookie('pll_updateC', 1, time() + 60 * 60 * 24 * 1);
        }
    }

    /*
     * check updates
     */
    private function form_checkUpdates()
    {
        global $wpdb;
        $settings = $this->getSettings();
        if (!isset($_COOKIE['pll_updateC']) || $_COOKIE['pll_updateC'] == '0' || $settings->updated) {
            $rep = "";
            $current = $settings->updated;
            if ($settings->purchaseCode == "") {
                $wpdb->update($wpdb->prefix . "vcht_settings", array('updated' => 1), array('id' => 1));
                setcookie('pll_updateC', 1, time() + 60 * 60 * 24 * 1);
            } else {
                $url = 'http://www.loopus-plugins.com/updates/update.php?checkUpdates=8329900&version=' . $settings->purchaseCode . '&ip=' . $_SERVER['SERVER_ADDR'] . '&url=' . get_site_url();
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $rep = curl_exec($ch);
                if ($rep == 'needupdate') {
                    $wpdb->update($wpdb->prefix . "vcht_settings", array('updated' => 1), array('id' => 1));
                    setcookie('pll_updateC', 2, time() + 60 * 60 * 24 * 1);
                } else if ($rep == 'updated') {
                    $wpdb->update($wpdb->prefix . "vcht_settings", array('updated' => 1), array('id' => 1));
                    setcookie('pll_updateC', 1, time() + 60 * 60 * 24 * 1);
                } else {
                    $wpdb->update($wpdb->prefix . "vcht_settings", array('updated' => 0), array('id' => 1));
                    setcookie('pll_updateC', 1, time() + 60 * 60 * 24 * 1);
                }
            }
        }
    }

    /*
     *  Ajax: Operator new message
     */

    public function operatorSay()
    {
        global $wpdb;
        $current_user = wp_get_current_user();
        $operatorID = $current_user->ID;

        $msg = esc_sql($_POST['msg']);
        $msg = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $msg);
        $domElement = esc_sql($_POST['domElement']);
        $url = esc_sql($_POST['url']);
        $logID = esc_sql($_POST['logID']);
        $wpdb->insert($wpdb->prefix . "vcht_messages", array('logID' => $logID, 'content' => stripslashes($msg), 'url' => $url, 'domElement' => $domElement, 'isOperator' => true, 'date' => date('Y-m-d h:i:s'), 'userID' => $operatorID));
        echo 1;
        die();
    }

    /*
     * Ajax : get currents chats
     */

    public function recoverChats()
    {
        global $wpdb;
        $current_user = wp_get_current_user();
        $operatorID = $current_user->ID;

        $rep = array();
        $rows = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_logs WHERE finished=0 AND operatorID=$operatorID");
        foreach ($rows as $value) {

            $userPic = "";
            if ($value->userID > 0) {
                $userPic = get_avatar($value->userID, 48);
            }
            $value->avatarImg = $userPic;
            $msgs = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_messages WHERE logID=$value->id");
            $value->messages = array();
            foreach ($msgs as $msg) {
                if ($msg->isOperator) {
                    $msg->avatarOperator = get_avatar($msg->userID, 48);
                    $user = get_userdata($msg->userID);
                    $username = $user->display_name;
                    if (!$username || $username == "") {
                        $username = $user->user_login;
                    }
                    $msg->username = $username;
                    $msg->content = nl2br(stripslashes($msg->content));
                }
                $value->messages[] = $msg;
                $wpdb->update($wpdb->prefix . "vcht_messages", array('isRead' => true), array('id' => $msg->id));
            }
            $rep[] = $value;
        }
        echo json_encode($rep);
        die();
    }

    /*
     * Ajax: transfer chat to operator
     */

    public function transferChat()
    {
        global $wpdb;
        $operatorID = esc_sql($_POST['operatorID']);
        $logID = esc_sql($_POST['logID']);
        $wpdb->update($wpdb->prefix . "vcht_logs", array('operatorID' => $operatorID, 'transfer' => true), array('id' => $logID));
        echo '1';
        die();
    }

    /*
     * Ajax : operator close chat
     */

    function closeChat()
    {
        $logID = $_POST['logID'];
        global $wpdb;
        $wpdb->update($wpdb->prefix . "vcht_logs", array('finished' => true), array('id' => $logID));
        echo '1';
        die();
    }

    /*
     * Ajax:  return operator current chats
     */

    public function check_operator_chat()
    {
        global $wpdb;
        $current_user = wp_get_current_user();
        $operatorID = $current_user->ID;
        $operator = $current_user->display_name;
        if (!$operator || $operator == "") {
            $operator = $current_user->user_login;
        }

        $rep = array();
        $rep['chatRequests'] = array();
        $rep['chats'] = array();
        $rep['chatsClosed'] = array();
        $rep['operators'] = array();
        $rep['transfers'] = array();
        $rep['users'] = array();

        // check new chat requests
        $rows = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_logs WHERE finished=0 AND operatorID=0");
        foreach ($rows as $value) {
            $wpdb->update($wpdb->prefix . "vcht_logs", array('operatorLastActivity' => date('Y-m-d h:i:s')), array('id' => $value->id));
            if (abs(strtotime(date('Y-m-d h:i:s')) - strtotime($value->lastActivity)) > 30) { // close chat
                $wpdb->update($wpdb->prefix . "vcht_logs", array('finished' => true), array('id' => $value->id));
            } else {
                $rep['chatRequests'][] = $value;
            }
        }

        // check online users
        $rows = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_users");
        foreach ($rows as $value){
            if (abs(strtotime(date('Y-m-d h:i:s')) - strtotime($value->lastActivity)) >60) { // remove user
                $wpdb->delete($wpdb->prefix . "vcht_users", array('id' => $value->id));
            } else {
                $logs = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_logs WHERE vcht_id=".$value->id." AND finished=0");
                $chkLog = false;
                foreach($logs as $log){
                    if (abs(strtotime(date('Y-m-d h:i:s')) - strtotime($log->lastActivity)) < 60) {
                        $chkLog = true;
                    }
                }
                $value->logExist = $chkLog;
                $rep['users'][] = $value;
            }
        }

        // check chats
        $rows = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_logs WHERE finished=0 AND operatorID=" . $operatorID . ' AND transfer=0');
        foreach ($rows as $value) {
            //operatorLastActivity
            $wpdb->update($wpdb->prefix . "vcht_logs", array('operatorLastActivity' => date('Y-m-d h:i:s')), array('id' => $value->id));
            if (abs(strtotime(date('Y-m-d h:i:s')) - strtotime($value->lastActivity)) > 20) { // close chat
                $wpdb->update($wpdb->prefix . "vcht_logs", array('finished' => true), array('id' => $value->id));
            }
            $msgs = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_messages WHERE logID=$value->id AND isOperator=0 AND isRead=0");
            $value->timePast = abs(strtotime(date('Y-m-d h:i:s')) - strtotime($value->date));
            $value->messages = array();
            $value->country = $value->country;
            $value->city = $value->city;
            foreach ($msgs as $msg) {
                $msg->content = nl2br(stripslashes($msg->content));
                $value->messages[] = $msg;
                $wpdb->update($wpdb->prefix . "vcht_messages", array('isRead' => true), array('id' => $msg->id));
            }
            $rep['chats'][] = $value;
        }

        // check closed chats
        if (strlen($_POST['currentChats']) > 0) {
            $currentChats = explode(',', $_POST['currentChats']);
            if (count($currentChats) > 0) {
                foreach ($currentChats as $currentChat) {
                    $rows = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_logs WHERE sent=0 AND finished=1 AND id=" . $currentChat . " LIMIT 1");
                    if (count($rows) > 0) {
                        $rep['chatsClosed'][] = $currentChat;
                        $wpdb->update($wpdb->prefix . "vcht_logs", array('sent' => true), array('id' => $currentChat));
                    }
                }
            }
        }

        // check transfers
        $rows = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_logs WHERE transfer=1 AND finished=0 AND operatorID=" . $operatorID);
        foreach ($rows as $value) {
            $wpdb->update($wpdb->prefix . "vcht_logs", array('transfer' => false), array('id' => $value->id));

            $msgs = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_messages WHERE logID=$value->id");
            foreach ($msgs as $msg) {
                $msg->content = nl2br(stripslashes($msg->content));
                if ($msg->isOperator) {
                    $user = get_userdata($msg->userID);
                    $username = $user->display_name;
                    if (!$username || $username == "") {
                        $username = $user->user_login;
                    }
                    $msg->username = $username;
                }
                $value->messages[] = $msg;
            }
            $rep['transfers'][] = $value;
        }
        // return online operators
        $wpdb->update($wpdb->prefix . "vcht_operators", array('lastActivity' => date('Y-m-d h:i:s')), array('userID' => $operatorID));
        $rowsO = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_operators");
        foreach ($rowsO as $value) {
            if (abs(strtotime(date('Y-m-d h:i:s')) - strtotime($value->lastActivity)) < 20) { // close chat
                $rep['operators'][] = $value;
            }
        }


//  echo '{"chatRequests":'. json_encode($rep['chatRequests'] ).',"chats":'.  json_encode($rep['chats'] ).'}';
        echo json_encode($rep);
        die();
    }

    /*
     * Ajax: operator accepts chat
     */

    public function acceptChat()
    {
        global $wpdb;
        $logID = esc_sql($_POST['logID']);
        $rows = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_logs WHERE id=" . $logID . " LIMIT 1");
        if ($rows[0]->operatorID == 0) {
            $current_user = wp_get_current_user();
            $operatorID = $current_user->ID;
            $wpdb->update($wpdb->prefix . "vcht_logs", array('operatorID' => $operatorID), array('id' => $logID));
            echo '1';
        } else {
            echo '0';
        }
        die();
    }

    /*
     * Ajax: operator Conexion
     */

    public function operatorConnect()
    {
        global $wpdb;
        $current_user = wp_get_current_user();
        $operatorID = $current_user->ID;
        $operator = $current_user->display_name;
        if (!$operator || $operator == "") {
            $operator = $current_user->user_login;
        }
        $rowsO = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_operators WHERE userID=$operatorID LIMIT 1");
        if (count($rowsO) > 0) {
            $wpdb->update($wpdb->prefix . "vcht_operators", array('lastActivity' => date('Y-m-d h:i:s')), array('userID' => $operatorID));
        } else {
            $wpdb->insert($wpdb->prefix . "vcht_operators", array('lastActivity' => date('Y-m-d h:i:s'), 'userID' => $operatorID, 'username' => $operator));
        }
        die();
    }

    /*
     * Ajax: operator disconnects
     */

    public function operatorDisconnect()
    {
        global $wpdb;
        $current_user = wp_get_current_user();
        $operatorID = $current_user->ID;
        $wpdb->delete($wpdb->prefix . "vcht_operators", array('userID' => $operatorID));
    }

    /*
     * Ajax: return a specific log infos
     */

    public function getLogChat()
    {
        global $wpdb;
        $current_user = wp_get_current_user();
        $operatorID = $current_user->ID;
        $rows = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_logs WHERE id=" . $_POST['logID'] . " LIMIT 1");
        if (count($rows) > 0) {
            if ($rows[0]->userID > 0) {
                $rows[0]->avatarImg = get_avatar($rows[0]->userID, 37);
            }
            $rowsM = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "vcht_messages WHERE logID=" . $_POST['logID'] . " ORDER BY id ASC");
            $rows[0]->messages = array();
            foreach ($rowsM as $value) {
                $value->content = nl2br(stripslashes($value->content));
                $rows[0]->messages[] = $value;
            }
            echo json_encode($rows[0]);
        }
        die();
    }

    /**
     * Main Instance
     *
     *
     * @since 1.0.0
     * @static
     * @return Main instance
     */
    public static function instance($parent)
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self($parent);
        }
        return self::$_instance;
    }

// End instance()

    /**
     * Cloning is forbidden.
     *
     * @since 1.0.0
     */
    public function __clone()
    {
        _doing_it_wrong(__FUNCTION__, __('Cheatin&#8217; huh?'), $this->parent->_version);
    }

// End __clone()

    /**
     * Unserializing instances of this class is forbidden.
     *
     * @since 1.0.0
     */
    public function __wakeup()
    {
        _doing_it_wrong(__FUNCTION__, __('Cheatin&#8217; huh?'), $this->parent->_version);
    }

}
